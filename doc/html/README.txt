----------------------------------
--- Zum Format der Documentary ---
----------------------------------

1. Alle konvertierten Seiten verwendend die folgende einleitende Zeile mit den html Definitonen:

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


2. Mit folgender Zeile wird die zentrale css Datei geladen, die das Gesamtlayout verwaltet:

<link href="../css/style.css" type="text/css" rel="stylesheet" />


3. Folgende Zeile, vor der "toptitle"-Überschrift plaziert erstellt den "table of contents" Kasten:

<a href="../index.html" class="home">table of contents</a>


4. Für die Gestaltung wurden in den konvertierten Dateien für alle Textbereiche "<div>"-Tags mit den
folgenden Optionen verwandt:

<div class="toptitle">	-  Gr. Überschrift ohne Einrückung	

<div class="headtitle">	-  Gr. Überschrift mit seitlicher Einrückung
	
<div class="subtitle">	-  Kl. Überschrift (nicht farbl. hinterlegt) mit seitlicher Einrückung	

<div class="content"> 	-  Für alle Fließtextelemente mit seitlicher Einrückung


5. Vor dem "<div>"-Tag, welches die Überschrift der Parameter einklammert, erstellt folgendes Element einen sog.
Anker, zu dem man dann von dem Fließtext aus mit dem Befehl "<a href="#Anker">Hier gehts zum Anker</a> gelangen
kann:  <a name="Anker"></a>

6. Nachfolgendes Element erzeugt die abschließende Trennlinie, das Minerva Symbol sowie die Angaben zu Autor und
Copyright. In den konvertierten Dateien ist es soweit immer direkt nach dem letzten abschließenden "<div>"-Tag
untergebracht:

<br>
<hr class="hr" />

<A href="http://www.cbs.mpg.de/index.xml"><img src="../images/minerva.bmp" border="0" style="margin-right:4px" align="left"></a>


<FONT style="font-family:Arial, Helvetica; font-size:8pt;"> 
Max Planck Institute for Human Cognitive and Brain Sciences. Further Information:
<A href="mailto:lipsia@cns.mpg.de">lipsia@cns.mpg.de</A>
<BR>Copyright &copy; 2001-2004 Max Planck Institute for Human Cognitive and Brain Sciences.
All rights reserved.

<br><br>


(7.) Das Gerüst der konvertierten Dateien sieht also in der Regel in etwa wie folgt aus:


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
<link href="../css/style.css" type="text/css" rel="stylesheet" />
</head>

<body>

<a href="../index.html" class="home">table of contents</a>
<div class="toptitle">Große Überschrift</div>

<div class="headtitle">Eingerückte Überschrift</div>

<div class="content">
Fließtext
</div>

<div class="subtitle">Unterpunkt1</div>

<div class="content">
Fließtext

<a href="Anker">Anker zu den Parametern</a>

</div>

<div class="subtitle">Unterpunkt2</div>

<div class="content">
Fließtext
</div>

<a name="Anker"></a>
<div class="headtitle">Parameter des Programmes</div>

<div class="content">
Fließtext
</div>

<br>
<hr class="hr" />

<A href="http://www.cbs.mpg.de/index.xml"><img src="../images/minerva.bmp" border="0" style="margin-right:4px" align="left"></a>


<FONT style="font-family:Arial, Helvetica; font-size:8pt;"> 
Max Planck Institute for Human Cognitive and Brain Sciences. Further Information:
<A href="mailto:lipsia@cns.mpg.de">lipsia@cns.mpg.de</A>
<BR>Copyright &copy; 2001-2007 Max Planck Institute for Human Cognitive and Brain Sciences.
All rights reserved.

<br><br>

</body></html>
