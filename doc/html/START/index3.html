<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title>Lipsia - Utilities</title>
<link href="../css/style.css" type="text/css" rel="stylesheet" />    
</head>
<body>

<a href="../index.html" class="home">table of contents</a>
<div class="toptitle">LIPSIA &nbsp;&nbsp;&nbsp; The Lipsia data format</div>


<div class="subtitle">The Lipsia data format</div>

<div class="content">
<p>This Lipsia data format originates from 
the <a href="http://www.cs.ubc.ca/labs/lci/vista/vista.html" target=_blank>Vista data format</a> which 
was developed by Art Pope and David Lowe at the
University of British Columbia.</p>

<p>Lipsia employs a data file format that allows any collection of images to be stored in a single file.
This allows, for example, to keep anatomical and functional images in one file.
Each file is divided into two parts. The first part, which contains
descriptions of the objects in the file, is represented entirely in
ASCII so that it can be easily viewed and even edited using a text
editor. For example, one can add a comment annotating an image by simply editing this part of the file. The second part of the file holds the image pixel values.</p>

<p>The Lipsia (Vista) file names should always have the extension
'.v'. Anatomical and functional data can be viewed using the program <a
href="../vsview/index.html">'vsview'</a> which is only a simple
viewer for Vista files, however. A more sophisticated and flexible way
of visualizing Lipsia
data is using the viewers <a href="../vlv/index.html">'vlv'</a>
and <a href="../vlview/index.html">'vlview'</a>.


</div>

<div class="subtitle">Display the ASCII header</div>

<div class="content">
<p>Using our example data file 'structural.v', the ASCII header can be shown doing</p>
</P> 

<p><tt><b><u>more</u> structural.v</b></tt><br></p>

<p>Then, the following ASCII header appears on the screen:</p>

<pre>
V-data 2 {
        image: image {
                data: 0
                length: 7180800
                nbands: 170
                nframes: 170
                nrows: 240
                ncolumns: 176
		bandtype: spatial
                repn: ubyte
                voxel: "1.000000 1.000000 1.500000"
                convention: natural
                orientation: axial
        }
</pre>

<p>The ASCII header contains the attributes of all images which are stored in the file. In our example 'structural.v', the file contains only 1 image. The image attributes shown in this example (data, length, nbands, nframes, nrows, ncolumns, bandtype, repn, voxel, convention, and orientation) are <u>required</u> for all images in Lipsia.</p>

<p>The attributes <font color=blue>nbands</font>, <font color=blue>nrows</font>, and <font color=blue>ncolumns</font> show the number of slices, rows, and columns of the image, respectively. The attribute <font color=blue>bandtype</font> shows if the bands contain slices (e.g. in structural images) or timesteps (e.g. in functional images). The attribute <font color=blue>repn</font> denotes the image data representation. Possible image representations are 'bit' (1-bit), 'ubyte' (8-bit integer), 'short' (16-bit integer), 'float' (32 bit floating point), and 'double' (64 bit floating point). In our example above, we have the representation 'ubyte', i.e., the image has integer values between 0 and 255.
The attribute <font color=blue>voxel</font> denotes the voxel size in mm (row, column, slice). In our example, we have a voxel size of 1x1x1.5mm. The attribute <font color=blue>convention</font> denotes the image convention. Possible values are 'natural' (L=L and R=R) and 'radiological' (L=R and R=L). Note that for Lipsia processing, all data must be stored in natural convention. The last attribute is the slice <font color=blue>orientation</font> which can be 'axial', 'sagittal', or 'coronal'. In our example, we have a set of 170 axial slices.</p>
</div>

<div class="subtitle">Changing the ASCII header</div>

<div class="content">
<p>The ASCII header can easily be changed using a text editor.
One can add, edit, and delete optional attributes or comments
annotating an image by simply editing this part of the file,
e.g. using the editor 'joe'. However,  a safer way of changing the
image attributes is to use the Lipsia program <a href="../vattredit/index.html">'vattredit'</a>. In our example, we could do the following:</p>

<tt><b><a href="../vattredit/index.html"><u>vattredit</u></a> -in structural.v -out outfile.v
-name "patient" -value "Paul Miller"
</b></tt><br><p>

<p> With this command we edit (or add) the attribute 'patient' and
give it the value 'Paul Miller'. Note that this is usually done for
all objects in the input file, unless an individual object is specified by
the option '-obj' when calling <a
href="../vattredit/index.html">'vattredit'</a>. However, in our
example data the input file contains only 1 image. The resulting
header of 'outfile.v' thus looks as follows:</p>


<pre>
V-data 2 {
        history: {
                <font color=blue>vattredit: "1.3; -obj -1 -name patient -value Paul Miller"</font>
        }
        image: image {
                data: 0
                length: 7180800
                nbands: 170
                nframes: 170
                nrows: 240
                ncolumns: 176
		bandtype: spatial
                repn: ubyte
                voxel: "1.000000 1.000000 1.500000"
                convention: natural
                orientation: axial
                <font color=blue>patient: "Paul Miller"</font>
        }
</pre>

<p>The changes in the header are marked in <font color=blue>blue</font>. After using Lipsia programs, the first part of the ASCII header shows the history of the processing steps. In our example,
the file 'outfile.v' is obtained after using the program <a href="../vattredit/index.html">'vattredit'</a>. The header shows the program version and all command-line options.</p>
</div>

<div class="subtitle">Anatomical MRI data</div>

<div class="content">

The Lipsia data format is able to store different images in a single file. 
These images are 'objects' of the Lipsia data file. In our example
data set, the anatomical data file 'structural.v' contains only one image, 
i.e., the file contains only one 'object'. This can be seen as
follows: </p>

<p><tt><b><a href="../vsview/index.html"><u>vsview</u></a> -in  structural.v</b></tt><br></p>

<img src="images/vxview.gif" border=0>

<p>
In anatomical data sets, bands correspond to image slices, whereas
rows and columns correspond to within-slice position. The anatomical
data set can also be viewed with
 
<p><tt><b><a href="../vlv/index.html"><u>vlv</u></a> -in structural.v</b></tt><br></p>

<img src="images/3Dorig1.gif" border=0>

<p> <b> Important:</b> Structural data must have the
orientation as shown in the figures above: Bands must be ordered
from dorsal to ventral, rows must be ordered from anterior to
posterior, and columns must be ordered from left to right. The latter
corresponds to the natural convention (L=L and R=R).

</div>



<div class="subtitle">Functional MRI data</div>

<div class="content">
In Lipsia, (4D-) functional MRI data are organized slightly
different from structural data. This enables a particularly fast data
processing.

<p> Functional MRI data are stored as a set of
several 'objects'. Each object represents an image slice,
where bands correspond to time steps and rows and columns again 
correspond to within-slice position.

Our example data contains six functional slices, i.e., the file
'data.v' contains six 'objects' called "Image". Calling 
'<a href="../vsview/index.html">vsview</a> -in data.v', the six
functional slices appear as images in the object list (see Figure
below).</p>

<img src="images/functional.gif" border=0>

<p>
Individual time steps of a functional data set can also be viewed like
this:

<p> 
<tt><b><a href="../vtimestep/index.html"><u>vtimestep</u></a> -in data.v -out tmp.v -step 0</b></tt><br><p>

<tt><b><a href="../vlview/index.html"><u>vlview</u></a> -in tmp.v</b></tt><br><p>

<img src="images/functional21.gif" border=0>

<p> Note that in the coronal and sagittal views only the six slices
are seen.

<p> <b> Important:</b> Before preprocessing, functional data must have the
orientation as shown in the figures above: <b>Objects must be ordered
 <u>from ventral to dorsal</u> (i.e. <u>upside down</u>). When importing (unpreprocessed) raw functional volumes
 using anatov or niftov, specify '-zflip true' in order to flip the slices.</b></p>
 
<p>In general, rows must be ordered
from anterior to posterior, and columns must be ordered from left to
right. The latter corresponds to the natural convention (L=L and R=R).</p>

<p>As described above, the header of the image file can be shown using the command 'more'. 
In our example, the ASCII header of 'data.v' contains information of
all images, i.e., of all six axial slices.</p>


<pre>
V-data 2 {
        image: image {
                data: 0
                length: 983040
                nbands: 120
                nframes: 120
                nrows: 64
                ncolumns: 64
		bandtype: temporal
                repn: short
                voxel: "3.000000 3.000000 4.500000"
                convention: natural
                orientation: axial
                MPIL_vista_0: " repetition_time=2000 packed_data=1 120 "
                ntimesteps: 120
                repetition_time: 2000
                slice_time: 600
        }
        image: image {
                data: 983040
                length: 983040
                nbands: 120
                nframes: 120
                nrows: 64
                ncolumns: 64
		bandtype: temporal
                repn: short
                voxel: "3.000000 3.000000 4.500000"
                convention: natural
                orientation: axial
                MPIL_vista_0: " repetition_time=2000 packed_data=1 120 "
                ntimesteps: 120
                repetition_time: 2000
                slice_time: 800
        }
        image: image {
                data: 1966080
                length: 983040
...
</pre>

<p>Because this dataset containes functional images, the attribute 'bandtype' has the value 'temporal' in all images.In addition to the required attributes described above, functional images must also contain the attributes 'MPIL_vista_0', 'ntimesteps', 'repetition_time', and 'slice_time'. The 
attribute <font color=blue>'MPIL_vista_0'</font> contains the repetition time (in our example 2000 ms) and the
number of functional scans (i.e. timesteps). In our example, we have 120 timesteps. 
The image attributes <font color=blue>'repetition_time'</font> and <font color=blue>'ntimesteps'</font> 
show also those numbers, i.e. the repetition time of 2000 ms and the number of 120 timesteps.
The attribute <font color=blue>'slice_time'</font> shows the exact slice acquisition time after the trigger.
In our example, the first (most ventral) slice was acquired 600 ms
after the trigger. The second slice was acquired 800 ms after the
trigger, and so on.</p>
</div>


<div class="subtitle">List of image attributes</div>

<div class="content">

<p>The following table gives a set of image attributes which are used in the Lipsia data format.</p>

<table summary="List of image attributes" border="1" cellspacing="0" cellpadding="5">
<tr><td>data</td><td>number</td><td><font color=red>required</font></td><td>position of the image in the binary data</td></tr>
<tr><td>length</td><td>number</td><td><font color=red>required</font></td><td>length of the binary image data</td></tr>
<tr><td>nbands</td><td>number</td><td><font color=red>required</font></td><td>number of bands (anatomical image: slices,
functional image: scans)</td></tr>
<tr><td>nframes</td><td>number</td><td><font color=red>required</font></td><td>number of frames (anatomical image: slices,
functional image: scans)</td></tr>
<tr><td>nrows</td><td>number</td><td><font color=red>required</font></td><td>number of rows</td></tr>
<tr><td>ncolumns</td><td>number</td><td><font color=red>required</font></td><td>number of columns</td></tr>
<tr><td>bandtype</td><td>spatial, temporal</td><td><font color=red>required</font></td><td>type of bands,
e.g. 'spatial' in structural images, 'temporal' in functional images</td></tr>
<tr><td>repn</td><td>bit, ubyte, short, long, float, double</td><td><font color=red>required</font></td><td>image
representation, i.e. ubyte (8bit) has gray values between 0 and 255</td></tr>
<tr><td>voxel</td><td>3 numbers</td><td><font color=red>required</font></td><td>image
voxel size (row, column, and slice thickness)</td></tr>
<tr><td>convention</td><td>natural, radiological</td><td><font color=red>required</font></td><td>image
convention ('natural' convention is required by many Lipsia programs)</td></tr>
<tr><td>orientation</td><td>axial, sagittal, coronal</td><td><font color=red>required</font></td><td>slice
orientation</td></tr>
<tr><td>MPIL_vista_0</td><td>text</td><td><font color=red>required in functional data</font></td><td>text containing
repetition time and the number of scans</td></tr>
<tr><td>ntimesteps</td><td>number</td><td><font color=red>required in functional data</font></td><td>number of functional scans
(timesteps)</td></tr>
<tr><td>repetition_time</td><td>number</td><td><font color=red>required in functional data</font></td><td>repetition time in ms</td></tr>
<tr><td>slice_time</td><td>number</td><td><font color=red>required in functional data</font></td><td>slice acquisition time in ms
(after trigger)</td></tr>
<tr><td>ca</td><td>3 numbers</td><td><font color=red>required in Talairach space</font></td><td>position of commissura anterior
in voxels</td></tr>
<tr><td>cp</td><td>3 numbers</td><td><font color=red>required in Talairach space</font></td><td>position of
commissura posterior in voxels</td></tr>
<tr><td>extent</td><td>3 numbers</td><td><font color=red>required in Talairach space</font></td><td>extent (size)
of the brain in mm</td></tr>
<tr><td>fixpoint</td><td>3 numbers</td><td><font color=red>required in Talairach space</font></td><td>fixpoint of
the image in voxels</td></tr>
<tr><td>talairach</td><td>atlas, neurological</td><td><font color=red>required in Talairach space</font></td>
<td>type of ca/cp image orientation ('atlas' is required by many Lipsia programs)</td></tr>
<tr><td>name</td><td>text</td><td><font color="FF7E00">required by some programs</font></td>
<td>name of the image</td></tr>
<tr><td>modality</td><td>text</td><td><font color="FF7E00">required by some programs</font></td>
<td>image modality, e.g. zmap</td></tr>
<tr><td>patient</td><td>text</td><td><font color=green>optional</font></td>
<td>name or description of the subject/patient</td></tr>
<tr><td>birth</td><td>DD.MM.YYYY</td><td><font color=green>optional</font></td>
<td>birth date of the subject/patient</td></tr>
<tr><td>sex</td><td>male, female</td><td><font color=green>optional</font></td>
<td>gender of the subject/patient</td></tr>
<tr><td>device</td><td>text</td><td><font color=green>optional</font></td>
<td>name of acquisition device, e.g. Siemens Trio</td></tr>
<tr><td>protocol</td><td>text</td><td><font color=green>optional</font></td>
<td>type of acquisition protocol, e.g. t1_mpr_axial_standard</td></tr>
<tr><td>sequence</td><td>text</td><td><font color=green>optional</font></td>
<td>type of acquisition sequence</td></tr>
<tr><td>CoilID</td><td>text</td><td><font color=green>optional</font></td>
<td>name or id of coil</td></tr>
<tr><td>date</td><td>DD.MM.YYYY</td><td><font color=green>optional</font></td>
<td>date of image acquisition</td></tr>
<tr><td>time</td><td>HH:MM:SS</td><td><font color=green>optional</font></td>
<td>time of image acquisition</td></tr>
<tr><td>boundingBox</td><td>special format</td><td><font color=green>optional</font></td>
<td>image bounding box</td></tr>
<tr><td>ori_nrows</td><td>number</td><td><font color=green>optional</font></td>
<td>number of rows in the original data set</td></tr>
<tr><td>ori_ncolumns</td><td>number</td><td><font color=green>optional</font></td>
<td>number of columns in the original data set</td></tr>
<tr><td>acquisitionOrientation</td><td>axial, sagittal, coronal</td><td><font color=green>optional</font></td>
<td>original orientation after image acquisition</td></tr>


</table>
</div>
<br>
<hr class="hr" />

<A href="http://www.cbs.mpg.de/index.xml"><img src="../images/minerva.bmp" border="0" style="margin-right:4px" align="left"></a>


<FONT style="font-family:Arial, Helvetica; font-size:8pt;"> 
Max Planck Institute for Human Cognitive and Brain Sciences. Further Information:
<A href="mailto:lipsia@cbs.mpg.de">lipsia@cbs.mpg.de</A>
<BR>Copyright &copy; 2007 Max Planck Institute for Human Cognitive and Brain Sciences.
All rights reserved.

<br><br>

</BODY></HTML>
