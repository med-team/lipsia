/****************************************************************
 *
 * anatov.c: converts Analyze 7.x (actually SPM) to Vista
 * 
 * Copyright (C) 2004 MPI of Cognitive Neuroscience, Leipzig
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Written by Sven Hessler, Apr. 2004, <sven@xafilac.de>
 *
 * $Id: anatov.c 3262 2008-04-11 14:56:04Z karstenm $
 *
 *****************************************************************/


#include "anatov.h"

#define ABS(x) ((x) < 0 ? -(x) : (x))

#define PROGNAME    "anatov"

extern char * getLipsiaVersion();
char program_id[] = "anatov: $Revision: 3262 $ $Date: 2008-04-11 16:56:04 +0200 (Fri, 11 Apr 2008) $ (C) MPI-CNS";


/* Analyze-hdr is big-endian. (Network-representation in TCP is big-endian.)
   Force conversion as needed.

   dl is a debuglevel. 
*/
void little_endian_hdr(struct dsr *hdr, int dl) {
  int swapint   = must_swap_int16(WRITE_HIGH_FIRST);
  int swapfloat = must_swap_float(WRITE_EXP_FIRST);

  int16_t ca[3];

  if (swapint) {
    debugfprintf(DL_EVENMORE, dl, stdout,
		 "%s: Swapping ints in hdr.\n", __FUNCTION__);
    hdr->hk.sizeof_hdr = ntohl(hdr->hk.sizeof_hdr);
    hdr->hk.extents    = ntohl(hdr->hk.extents);

    hdr->dime.dim[0] = ntohs(hdr->dime.dim[0]);
    hdr->dime.dim[1] = ntohs(hdr->dime.dim[1]);
    hdr->dime.dim[2] = ntohs(hdr->dime.dim[2]);
    hdr->dime.dim[3] = ntohs(hdr->dime.dim[3]);
    hdr->dime.dim[4] = ntohs(hdr->dime.dim[4]);

    hdr->dime.datatype = ntohs(hdr->dime.datatype);

    /* swap ant. commissure */
    memcpy(ca, hdr->hist.originator, 3*sizeof(ca[0]));
    ca[0] = ntohs(ca[0]);
    ca[1] = ntohs(ca[1]);
    ca[2] = ntohs(ca[2]);
    memcpy(hdr->hist.originator, ca, 3*sizeof(ca[0]));
  }

  if (swapfloat) {
    debugfprintf(DL_EVENMORE, dl, stdout,
		 "%s: Swapping floats in hdr.\n", __FUNCTION__);
    swap_float(&hdr->dime.pixdim[0]);
    swap_float(&hdr->dime.pixdim[1]);
    swap_float(&hdr->dime.pixdim[2]);
    swap_float(&hdr->dime.pixdim[3]);
    swap_float(&hdr->dime.pixdim[4]);

    swap_float(&hdr->dime.vox_offset);
    swap_float(&hdr->dime.roi_scale);
  }
} /* little_endian_hdr() */


int set_aux_hdr_values(VImage img, char** lines, int nlines, int dl) {
  int i=0, keylen = 0;
  char *key = NULL, *val = NULL;
  
  /* every line contain a string consisting of "<key>: <value>" 
     colon space ": " is the delimiter between key and value 
   */

  for (i = 0; i < nlines; i++) {

    val = (char *)index(lines[i], 58);	/* perl -e'print ord ":"' */
    if (val != NULL) {
      keylen = strlen(lines[i]) - strlen(val) + 1;

      key = VMalloc(sizeof(char) * keylen);
      bzero(key, keylen);
      strncpy(key, lines[i], keylen - 1);

      /* val points ATM to ": xxxx", thus move pointer 2chars right: */
      val = val + 2;
      debugfprintf(DL_MOST, dl, stdout,
		   "%s: set ImageAttr '%s' => '%s'.\n", __FUNCTION__, key, val);
    
      VSetAttr(VImageAttrList(img), key, NULL, VStringRepn, val);

      keylen = 0;
    }
  }
  
  return i;
}


int read_aux_file(FILE *f, char** hash, int dl) {
#define MAX_LINE_LENGTH 255
  int i = 0, slen = 0;
  char line[MAX_LINE_LENGTH];
  char *res = NULL;
  
  while (!feof(f)) {
    bzero(line, 255);
    res = fgets(line, MAX_LINE_LENGTH, f);
    if (res != NULL) {
      slen = strlen(line);
      hash[i] = VMalloc(sizeof(char) * slen);
      bzero(hash[i], slen);
      memcpy(hash[i], line, slen - 1); /* do not copy EOL */

      i++;
      slen = 0;
      res = NULL;
    }
  }

  return i;
}


VImage *read_images(FILE *f, long ncols, long nrows, long nbands, long ts,
		    long ofset, VRepnKind repn, int *nimages, 
                    VBoolean xendian, VBoolean flipx, VBoolean flipy, VBoolean flipz, int dl) {

  VImage *img = NULL, curr_img = NULL;
  VImage *tmp = NULL;
  VImage  temp = NULL;
  int     img_count = 0, b, bb, r, c;
  int     i=0, ii=0, j=0, k=0, nframes = 0;
  VBand   src_band, dest_band;

  VPointer src = NULL, dest = NULL;
  VPointer dp = NULL;
  size_t nvox = 0;
  size_t destlen = 0, srclen = 0;

  /* number of images */
  *nimages = ts;

  /* Allocate images for each slice */
  img = VMalloc(sizeof(struct V_ImageRec) * (*nimages));

  /* Loop through all slices */
  for (i = 0; i < *nimages; i++) {

    img[i] = VCreateImage(nbands, nrows, ncols, repn);
    nvox = nbands * nrows * ncols;
 
    curr_img = img[i];
    dp = VPixelPtr(curr_img, 0, 0, 0);

    fseek(f, ofset, 0);
    fread(dp, nvox*VRepnSize(repn), 1, f); 

    if(! (src = VImageData(curr_img))) {
      VError("%s: Couldn't retrieve image-data", __FUNCTION__);
    }

    srclen = nvox * VPixelSize(curr_img);
    destlen = srclen; 
    dest = src;

    /* analyze data is big-endian: */ 
    if (xendian) VUnpackData(repn, nvox, src, VMsbFirst, &destlen, &dest, NULL); 

    ofset = ofset + nvox*VRepnSize(repn);

    /* Copy of original image */
    temp = VCopyImage(img[i], NULL, VAllBands);

    /* X flipping */
    if (flipx) {    
      debugfprintf(DL_EVENMORE, dl, stdout,
		   "%s: Flip x axis.\n", __FUNCTION__);
      VFillImage(temp,VAllBands,0);    
      VFlipImage(img[i],temp,VAllBands,FALSE);
      VCopyImage(temp,img[i],VAllBands);
    }

    /* Y flipping */    
    if (flipy) {
      debugfprintf(DL_EVENMORE, dl, stdout,
		   "%s: Flip y axis.\n", __FUNCTION__);
      VFillImage(temp,VAllBands,0);   
      VFlipImage(img[i],temp,VAllBands,TRUE);
      VCopyImage(temp,img[i],VAllBands);
    }   
 
    /* Z flipping */
    if (flipz) {
      debugfprintf(DL_EVENMORE, dl, stdout,
		   "%s: Flip z axis.\n", __FUNCTION__);
      VFillImage(temp,VAllBands,0); 
      bb = VImageNBands(temp)-1;
      for (b=0; b<VImageNBands(temp); b++) {
	for (r=0; r<VImageNRows(temp); r++) {
	  for (c=0; c<VImageNColumns(temp); c++) 
	    VSetPixel(temp, bb, r, c, VGetPixel(img[i], b, r, c)); 
	}
	bb--;
      }
      VCopyImage(temp,img[i],VAllBands);
    }
  }

  /* destroy temp image */
  VDestroyImage(temp);

  /* functional re-ordering */
  if (ts > 1) {
    
    tmp = VMalloc(sizeof(struct V_ImageRec) * nbands);    
      
    /* re-ordering the bands */
    for (i = 0; i < nbands; i++){ 
      tmp[i] = VCreateImage((*nimages), nrows, ncols, repn);      
      for (k = 0; k < *nimages; k++) 
	VCopyBand(img[k], i, tmp[i], k);      
    } 
  
    for (k = 0; k < *nimages; k++) 
      VDestroyImage(img[k]);
    img = tmp;
    *nimages = nbands;
  } 
  
  return img;
}
 

void convert_imgs(char *infname, FILE *out_file, VShort orientation, VBoolean flipx, VBoolean flipy, VBoolean flipz, VShort precision, VFloat black, VFloat white, VFloat tr, int dl) {
  FILE	      *in_file = NULL;
  char	      chartmp[128], mpilvista[128];
  char	      *cdata;
  struct dsr  anahd;
  VImage      *img = NULL, tmp = NULL;
  int         nimages = 0;
  VRepnKind   repn = VUnknownRepn;
  VImage      dest = NULL;
  int         nbands, nrows, ncols;
  int         i, j, k, nlines = 0;
  int         ii = 0;
  float       *st = NULL;
  char        *lines[255];
  VBoolean    auxfile = FALSE, xendian=FALSE;
  VDouble     u=0;


/* get header: */
  cdata = strrchr(infname, '.');
  if (cdata) { 
    switch ( *(cdata+1)) {
    case 'h':
    case 'H':
    case 'i':
    case 'I':
      *cdata = 0;
      break;
    }
  }

  sprintf(chartmp, "%s.hdr", infname);
  in_file = fopen(chartmp, "r");
  if (in_file == NULL) { 
    VError("%s: Unable to open HDR file \"%s\"", __FUNCTION__, chartmp);
  }
  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Read header file '%s'\n", __FUNCTION__, chartmp);
  fread(&anahd, 1, sizeof(struct dsr), in_file);
  fclose(in_file);
   
  switch (anahd.dime.datatype) { 
  case  DT_UNSIGNED_CHAR: 
    repn = VUByteRepn;   
    break;
  case  DT_SIGNED_SHORT: 
    repn = VShortRepn;  
    break;
  case DT_FLOAT: 
    repn = VFloatRepn;
    break;
  default: repn = VUnknownRepn; break;
  }

  debugfprintf(DL_MORE, dl, stdout,
		 "%s: Image representation \"%s\" (%i)\n", __FUNCTION__, VRepnName(repn), anahd.dime.datatype);

  /* If no repn could be found */
  if (repn == VUnknownRepn ) {  
    xendian=TRUE;    
    little_endian_hdr(&anahd, dl);
    
    switch (anahd.dime.datatype) { 
    case  DT_UNSIGNED_CHAR: 
      repn = VUByteRepn;   
      break;
    case  DT_SIGNED_SHORT: 
      repn = VShortRepn;  
      break;
    case DT_FLOAT:
      repn = VFloatRepn; 
      break;
    default: repn = VUnknownRepn; break;
    } 
  } 

  debugfprintf(DL_MORE, dl, stdout,
		 "%s: Image representation \"%s\" (%i)\n", __FUNCTION__, VRepnName(repn), anahd.dime.datatype);

  if (repn == VUnknownRepn ) {
    VError("%s: Image representation \"%s\" (%i) not supported. Sorry",
	   __FUNCTION__, VRepnName(repn), anahd.dime.datatype);
  }

  /*
   * read auxiliary file
   */
  auxfile = FALSE;
  sprintf(chartmp,"%s.txt",infname); 
  in_file = fopen(chartmp, "r"); 
  if (in_file != NULL) {
    auxfile = TRUE;
    debugfprintf(DL_MORE, dl, stdout,
		 "%s: Read auxiliary file '%s'\n", __FUNCTION__, chartmp);
    nlines = read_aux_file(in_file, lines, dl); 
    fclose(in_file); 
  }

  /*
   * Several images in functional images only 
   */
  if (repn != VShortRepn && anahd.dime.dim[4]>1)
    VWarning(" time series which are not short");
  if (anahd.dime.dim[3]>50 && !flipz)
    VWarning("The dataset looks like a 3D data set because it has more than 50 slices. Check if you really want to flip the z axis. Please check the image orientation");
  if (anahd.dime.dim[3]<50 && flipz)
    VWarning("The dataset has less than 50 slices. In Lipsia, 2D slices are ordered upside down. It might be useful to set '-zflip true'. Please check the image orientation");

  /*
   * check tr for functional data
   */
  if (anahd.dime.dim[4]>1) {
    if (tr > 32) VWarning(" Check if repetition time is specified in seconds");
    if (tr < 0.000001) VError(" specify repetition time");
    if ( ABS(tr - anahd.dime.pixdim[4])>0.000001 ) {
      VWarning(" TR in header is different from specified TR in command line");
      fprintf(stderr," Using TR from command line TR=%1.2f s (TR in header is %1.2f s)\n",tr,anahd.dime.pixdim[4]);
    }
  }    

  /*
   * get data
   */
  sprintf(chartmp,"%s.img",infname);
  in_file = fopen(chartmp, "r");
  if (in_file == NULL) {
    VError("%s: Unable to open IMG file \"%s\"", __FUNCTION__, chartmp);
  }

  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Read image file '%s'\n", __FUNCTION__, chartmp);
  img = read_images(in_file, anahd.dime.dim[1], anahd.dime.dim[2], 
		    anahd.dime.dim[3], anahd.dime.dim[4],
		    anahd.dime.vox_offset, repn, &nimages, xendian, flipx, flipy, flipz, dl);
  fclose(in_file);

  if (nimages == 0) {
    VError("%s: Could not read input file", __FUNCTION__);
  }

  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Convert %d images.\n", __FUNCTION__, nimages);


  for (ii=0; ii < nimages; ii++) {

    /*
     * Convert the anatomical image to ubyte
     */
    if (VPixelRepn(img[ii]) == VShortRepn && precision == 0 && anahd.dime.dim[4]<=1) {
      tmp = VContrastShortUByte(img[ii], (int)0, (int)255, black / 100.0, white / 100.0);
      VDestroyImage(img[ii]);
      img[ii]=tmp;
    } 

    debugfprintf(DL_LITTLE, dl, stdout,
		 "%s: Converting image no: %2d\n", __FUNCTION__, ii + 1);
    debugfprintf(DL_MORE, dl, stdout,
		 "%s: Representation of image no: %2d is \"%s\".\n", __FUNCTION__, ii, VRepnName(repn));
    debugfprintf(DL_MORE, dl, stdout,
		 "%s: Dimensions of Image no: %2d (X,Y,Z,TS) = %d,%d,%d,%d\n", 
		 __FUNCTION__, ii, anahd.dime.dim[1], anahd.dime.dim[2],
		 anahd.dime.dim[3], anahd.dime.dim[4]);

    /*
     * write header of new vista file
     */
    sprintf(chartmp,"%f %f %f",ABS(anahd.dime.pixdim[1]),ABS(anahd.dime.pixdim[2]),ABS(anahd.dime.pixdim[3]));
    VSetAttr(VImageAttrList(img[ii]), VhdrPixDim, NULL, VStringRepn, chartmp );
    VSetAttr(VImageAttrList(img[ii]), "convention", NULL, VStringRepn, "natural");
    /* VSetAttr(VImageAttrList(img[ii]), "name",NULL,VStringRepn,anahd.dime.descrip); */
    if (orientation==0) VSetAttr(VImageAttrList(img[ii]), "orientation", NULL, VStringRepn, "axial");
    if (orientation==1) VSetAttr(VImageAttrList(img[ii]), "orientation", NULL, VStringRepn, "sagittal");
    if (orientation==2) VSetAttr(VImageAttrList(img[ii]), "orientation", NULL, VStringRepn, "coronal");
    if (VPixelRepn(img[ii]) == VShortRepn && anahd.dime.dim[4]>1) {
      sprintf(mpilvista," repetition_time=%d packed_data=1 %d ",(VLong)(tr*1000.0),VImageNBands(img[ii]));
      VSetAttr(VImageAttrList(img[ii]), "MPIL_vista_0", NULL, VStringRepn, (VString)mpilvista );
      VSetAttr(VImageAttrList(img[ii]), "ntimesteps", NULL, VShortRepn, VImageNBands(img[ii]) );
      VSetAttr(VImageAttrList(img[ii]), "repetition_time",NULL,VLongRepn,(VLong)(tr*1000.0));
      VSetAttr(VImageAttrList(img[ii]), "bandtype", NULL, VStringRepn, "temporal" );
    } else {
      VSetAttr(VImageAttrList(img[ii]), "bandtype", NULL, VStringRepn, "spatial" );
    }

    if (auxfile == TRUE)
      set_aux_hdr_values(img[ii], lines, nlines, dl);
  }


  /* write data file: */
  if (VWriteImages(out_file, NULL, nimages, img) == 0) {
    VError("%s: Could not create output file", __FUNCTION__);
  }
  debugfprintf(DL_LITTLE, dl, stdout, 
	       "%s: Writing output file.\n", __FUNCTION__);


  /* cleanup: */
  for (i=0; i<nimages; i++) {
    VDestroyImage(img[i]);
  }

  for (i=0; i<nlines; i++) {
    if (lines[i] != NULL)
      VFree(lines[i]);
  }

} /* convert_imgs() */


int main (int argc, char **argv) {

  FILE		*out_file = NULL;
  static VShort debuglevel=0; /* DL_LITTLE; */
  static char	*infname = NULL;
  static VShort orientation = 0;
  static VBoolean flipx=FALSE;
  static VBoolean flipy=FALSE;	
  static VBoolean flipz=FALSE;
  static VShort precision = 0;
  static VFloat black = 0.1;      /* lower histogram cut-off for contrast scaling */
  static VFloat white = 0.1;      /* upper histogram cut-off for contrast scaling */
  static VFloat tr = 0;
 
  static VDictEntry OrientationDict[] = {
    { "axial"   , 0},
    { "sagittal", 1},
    { "coronal" , 2},
    { NULL }
  };
 
  static VDictEntry PrecisionDict[] = {
    { "auto"    ,  0 },
    { "original",  1 },
    { NULL }
  };
  
  static VOptionDescRec options[] = {
    { "in", VStringRepn, 1,&infname, VRequiredOpt, 
      NULL, "Input file" },
    { "precision", VShortRepn,   1, &precision, VOptionalOpt,  PrecisionDict,
      "precision of anatomical data"},
    { "black", VFloatRepn,   1, &black,     VOptionalOpt, NULL,   
      "lower histogram cutoff for contrast scaling"},
    { "white", VFloatRepn,   1, &white,     VOptionalOpt, NULL,   
      "upper histogram cutoff for contrast scaling"},
    { "xflip", VBooleanRepn, 1, &flipx, VOptionalOpt, NULL,
      "flip x axis" },    
    { "yflip", VBooleanRepn, 1, &flipy, VOptionalOpt, NULL,
      "flip y axis" },    
    { "zflip", VBooleanRepn, 1, &flipz, VOptionalOpt, NULL,
      "flip z axis" },
    { "tr",VFloatRepn,1,(VPointer) &tr,VOptionalOpt,NULL,
      "repetition time in seconds"},
    { "orientation", VShortRepn, 1, &orientation, VOptionalOpt, OrientationDict,
      "orientation of slices" }   
    /* { "verbose", VShortRepn, 1, &debuglevel, VOptionalOpt, NULL,
      "debuglevel"} */
  };
  char prg[50];	
  sprintf(prg,"anatov V%s", getLipsiaVersion());
  
  fprintf (stderr, "%s\n", prg);

  VParseFilterCmd(VNumber(options), options, argc, argv, NULL, &out_file);
  VSetProgramName(PROGNAME);

  convert_imgs(infname, out_file, orientation, !flipx, !flipy, !flipz, precision, black, white, tr, debuglevel);

  fprintf( stderr, "%s: done.\n", argv[0]);
  return 0;
} /* main */
