/****************************************************************
 *
 * anatov.h: converts Analyze 7.x (actually SPM2) to Vista
 * 
 * Copyright (C) 2004 MPI of Cognitive Neuroscience, Leipzig
 *
 * Sven Hessler, Apr. 2004 (sven@xafilac.de)
 * 
 *****************************************************************/

#ifndef ANATOV_H
#define ANATOV_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#ifndef __LCLINT__
#include <netinet/in.h>   /* for byte-swapping */
#endif /*__LCLINT__*/

/* From the Vista library: */
#include <viaio/Vlib.h>
#include <viaio/VImage.h>
#include <viaio/file.h>
#include <viaio/mu.h>
#include <viaio/option.h>
#include <via/via.h>


/* Attention! Several versions are floating around. I need the one w/
   "float roi_scale" at byte-offset 72 in image_dimension. */
#include "dbh.h"
#include "tools.h"


#define DL_LITTLE       1     /* elementary progress. */
#define DL_MORE         2     /* converted values */
#define DL_EVENMORE     3     /* detailed progress */
#define DL_MOST         4
#define DL_SICK         5

#define VhdrPixDim       "voxel"

void little_endian_hdr(struct dsr *hdr, int dl);
int  read_aux_file(FILE *f, char **hash, int dl);
int  set_aux_hdr_values(VImage img, char** lines, int nlines, int dl);
float* ReadSlicetimes(VString slicetime);

void convert_imgs(char *infname, FILE *out_file, VShort orientation, VBoolean flipx, VBoolean flipy, VBoolean flipz, VShort precision, VFloat black, VFloat white, VFloat TR, int dl);


#endif
