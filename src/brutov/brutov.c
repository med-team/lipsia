/*
 * brutov: convert Bruker to Vista image
 * version 1.0 (01/10/96)
 * comments to: F. Kruggel (E-mail kruggel@cns.mpg.de)
 *
 * version 1.01 (21/11/96): changes for Bruker format 1.0 incorporated
 * version 1.02 (23/11/96): changes for Bruker format 1.1 incorporated
 * version 1.03 (10/01/97): select datasets from an experiment, angles added
 * version 1.04 (13/01/97): changes for Bruker format 1.2 incorporateid
 * version 1.05 (04/02/97): changes for new Bruker software
 * version 1.06 (16/02/97): changes for multislice datasets, cleanup
 * version 1.10 (24/03/97): direct conversion from Bruker format
 * version 1.11 (21/06/97): serial number in title of functional data
 * version 1.12 (16/08/97): new coordinate system introduced
 * version 1.13 (13/01/99): changes for new MPIL format incorporated
 * version 1.14 (16/05/99): no of files increased to 1000
 * version 1.15 (04/07/99): changes for MPIL strings
 *
 */

/* From the Vista library: */
#include "viaio/Vlib.h"
#include "viaio/VImage.h"
#include "viaio/file.h"
#include "viaio/mu.h"
#include "viaio/option.h"

/* From the standard C library: */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include <dirent.h>
#include <values.h>

double fov[100];		/* field of view in 3d */
double dx, dy, dz;		/* real world voxel dimensions */
double ang[100];		/* tilt angles of slice stack */
double loc, gap, thick;		/* real world slice position */
double sta[100];		/* slice time array */
int nx, ny, nz, rdir;		/* image dimensions */
int nr = 0, ns = 0, nips = 1;	/* repitition count (in time-points) and images per slice */
int pixsize = 4;		/* byte size of a voxel */
int rep = 4;			/* that many images are take during a condition */
int mode = 0;			/* denotes MPIL sequence */
int ips = 1;			/* images per slice */
int dims[3];
char *condition = 0;
char pat[128], bday[128], date[128], method[64], wtype[64],
	ori[128], sex[128], study[128], scan[128], exp[128];
char *desc[10];		/* description: cross your fingers and hope it will fit */

struct param {
	char	*file;
	char	*name;
	int	type;
	void	*dest;
};

#define STRING		1
#define DOUBLE		2
#define ENUM		3
#define INT		4
#define INT_ARRAY	5
#define DOUBLE_ARRAY	6
#define STRING_ARRAY	7

#define NOTFOUND	0
#define FOUND		1
#define FATAL		2

#define NATIVE_MODE	0
#define MPIL_MODE	1
#define MPIL_GEO_MODE	2

struct param Bruker_table[] =
{
	{ "subject",	"SUBJECT_study_name",	STRING,		(void *)study	},
	{ "subject",	"SUBJECT_id",		STRING,		(void *)pat	},
	{ "subject",	"SUBJECT_dbirth",	STRING,		(void *)bday	},
	{ "subject",	"SUBJECT_sex",		STRING,		(void *)sex	},
	{ "subject",	"SUBJECT_date",		STRING,		(void *)date	},
	{ "%d/imnd",	"IMND_scan_name",	STRING,		(void *)scan	},
	{ "%d/imnd",	"IMND_method",		STRING,		(void *)method	},
	{ "%d/gui",	"GUI_fov_read",		DOUBLE,		(void *)&fov[0]	},
	{ "%d/gui",	"GUI_fov_phase1",	DOUBLE,		(void *)&fov[1]	},
	{ "%d/gui",	"GUI_fov_phase2",	DOUBLE,		(void *)&fov[2]	},
	{ "%d/gui",	"GUI_slice_dir_iso",	ENUM,		(void *)ori	},
	{ "%d/gui",	"GUI_slice_angle_1",	DOUBLE,		(void *)&ang[0]	},
	{ "%d/gui",	"GUI_slice_angle_2",	DOUBLE,		(void *)&ang[1]	},
	{ "%d/gui",	"GUI_read_angle",	DOUBLE,		(void *)&ang[2]	},
	{ "%d/gui",	"GUI_slice_thick_iso",	DOUBLE,		(void *)&thick	},
	{ "%d/gui",	"GUI_slice_gap_iso",	DOUBLE,		(void *)&gap	},
	{ "%d/gui",	"GUI_n_slices_iso",	INT,		(void *)&ns	},
	{ "%d/gui",	"GUI_slice_position",	DOUBLE,		(void *)&loc	},
	{ "%d/gui",	"GUI_read_dir",		INT,		(void *)&rdir	},
	{ "%d/pdata/1/reco", "RECO_size",	INT_ARRAY,	(void *)dims	},
	{ "%d/pdata/1/reco", "RECO_wordtype",	ENUM,		(void *)wtype	},
	{ "%d/acqp", 	"NR",			INT,		(void *)&nr	},
	{ 0 },
};
struct param MPIL_table[] =
{
	{ "subject",	"SUBJECT_study_name",	STRING,		(void *)study	},
	{ "subject",	"SUBJECT_id",		STRING,		(void *)pat	},
	{ "subject",	"SUBJECT_dbirth",	STRING,		(void *)bday	},
	{ "subject",	"SUBJECT_sex",		STRING,		(void *)sex	},
	{ "subject",	"SUBJECT_date",		STRING,		(void *)date	},
	{ "%d/imnd",	"IMND_scan_name",	STRING,		(void *)scan	},
	{ "%d/imnd",	"IMND_method",		STRING,		(void *)method	},
	{ "%d/imnd",	"MPIL_GED_fov",		DOUBLE_ARRAY,	(void *)fov	},
	{ "%d/imnd",	"MPIL_GED_slice_dir",	ENUM,		(void *)ori	},
	{ "%d/imnd",	"MPIL_GED_angles",	DOUBLE_ARRAY,	(void *)ang	},
	{ "%d/imnd",	"MPIL_GED_slice_thick",	DOUBLE,		(void *)&thick	},
	{ "%d/imnd",	"MPIL_GED_slice_gap",	DOUBLE,		(void *)&gap	},
	{ "%d/imnd",	"MPIL_GED_n_slices",	INT,		(void *)&ns	},
	{ "%d/imnd",	"MPIL_GED_slice_position", DOUBLE,	(void *)&loc	},
	{ "%d/imnd",	"MPIL_GED_read_dir", 	INT,		(void *)&rdir	},
	{ "%d/pdata/1/reco", "RECO_size",	INT_ARRAY,	(void *)dims	},
	{ "%d/pdata/1/reco", "RECO_wordtype",	ENUM,		(void *)wtype	},
	{ "%d/acqp", 	"NR",			INT,		(void *)&nr	},
	{ 0 },
};
struct param MPIL_GEO_table[] =
{
	{ "subject",	"SUBJECT_study_name",	  	STRING,		(void *)study	},
	{ "subject",	"SUBJECT_id",		  	STRING,		(void *)pat	},
	{ "subject",	"SUBJECT_dbirth",	  	STRING,		(void *)bday	},
	{ "subject",	"SUBJECT_sex",		  	STRING,		(void *)sex	},
	{ "subject",	"SUBJECT_date",		  	STRING,		(void *)date	},
	{ "%d/imnd",	"IMND_scan_name",	  	STRING,		(void *)scan	},
	{ "%d/imnd",	"IMND_method",		  	STRING,		(void *)method	},
	{ "%d/imnd",	"MPIL_GEO_fov",		  	DOUBLE_ARRAY,	(void *)fov	},
	{ "%d/gui",	"GUI_slice_dir_iso",		ENUM,		(void *)ori	},
	{ "%d/imnd",	"MPIL_GEO_slice_angle",	  	DOUBLE_ARRAY,	(void *)ang	},
	{ "%d/imnd",	"MPIL_GEO_slice_thick",	  	DOUBLE,		(void *)&thick	},
	{ "%d/gui",	"GUI_slice_gap_iso",		DOUBLE,		(void *)&gap	},
	{ "%d/imnd",	"MPIL_GEO_n_slices",		INT,		(void *)&ns	},
	{ "%d/gui",	"GUI_slice_position",		DOUBLE,		(void *)&loc	},
	{ "%d/pdata/1/reco", "RECO_size",		INT_ARRAY,	(void *)dims	},
	{ "%d/pdata/1/reco", "RECO_wordtype",		ENUM,		(void *)wtype	},
	{ "%d/imnd", 	"MPIL_n_repetitions",		INT,		(void *)&nr	},
	{ "%d/imnd", 	"MPIL_images_per_slice",	INT,		(void *)&ips	},
	{ "%d/imnd", 	"MPIL_experiment_type",		ENUM,		(void *)exp	},
	{ "%d/imnd", 	"MPIL_vista",			STRING_ARRAY,	(void *)desc	},
	{ "%d/imnd", 	"MPIL_slice_time_array",	DOUBLE_ARRAY,	(void *)sta	},
	{ 0 },
};
struct param mode_table[] =
{
	{ "%d/imnd",	"MPIL_GED_mode",	INT,		(void *)&mode	},
	{ "%d/imnd",	"MPIL_GEO_fov",		DOUBLE_ARRAY,	(void *)fov	},
};

VImage VScaleIntensity(VImage src, double white, double black, int fold)
/* scale any input image to VUByte,
   mapping white (black) percent of the voxel to white (black) */
{
	int x, y, z, zi, nx, ny, nz, i, range, maxshort;
	unsigned int lb, ub, limit, sum, *histo;
	double m, b, max, min, mean, var, v;
	VImage dst;

	maxshort = (int)(VRepnMaxValue(VShortRepn));
	histo = (unsigned int *)VCalloc(maxshort, sizeof(unsigned int));
	nx = VImageNColumns(src);
	ny = VImageNRows(src);
	nz = VImageNBands(src);

	if (white < 0 || white > 100 || black < 0 || black > 100 || white+black >= 100)  {
		fprintf(stderr, "VScaleIntensity: illegal percentage given.\n");
		return 0;
	};

	/* first pass: find maximum and minimum values */
	VImageStats(src, VAllBands, &min, &max, &mean, &var);
	if (max == min) {
		fprintf(stderr, "VScaleIntensity: illegal data in image.\n");
		return 0;
	};
	b = min;
	m = (max-min) / (double)maxshort;

	/* second pass: build a histogram*/
	for (z = 0; z < nz; z++)  {
		for (y = 0; y < ny; y++)  {
			for (x = 0; x < nx; x++)  {
				v = VGetPixel(src, z, y, x);
				i = (int)((v-b)/m+0.5);
				histo[i]++;
			};
		};
	};

	/* throw away pc percent of the voxel below lb and pc percent above ub */
	limit = (black * nx * ny * nz) / 100;
        lb = 0; sum = 0;
        for (i = 0; i < maxshort; i++)  {
        	sum += histo[i];
        	if (sum >= limit) { lb = i; break; };
        };
	limit = (white * nx * ny * nz) / 100;
        ub = maxshort-1; sum = 0;
        for (i = maxshort-1; i >= 0; i--)  {
        	sum += histo[i];
        	if (sum >= limit) { ub = i; break; };
        };
	min = lb*m+b;
	max = ub*m+b;

	/* third pass: create and convert image */
	dst = VCreateImage(nz, ny, nx, VUByteRepn);
	if (dst == 0) return 0;

	range = 256;
        m = range / (max - min);
        b = range - (m * max);
	for (z = 0; z < nz; z++)  {
		for (y = 0; y < ny; y++)  {
			for (x = 0; x < nx; x++)  {
				v = VGetPixel(src, z, y, x);
				i = (int)(v * m + b + 0.5);
                        	if (i < 0) i = 0;
                        	else if (i >= range) i = range-1;
                        	zi = z-fold; if (zi < 0) zi += nz;
                        	VSetPixel(dst, zi, y, x, i);
                	};
                };
        };
        VFree(histo);
        return dst;
}

static int Decode (VStringConst str)
/* decode a numerical index value from the command line */
{
	VLong value;

	if (! VDecodeAttrValue (str, NULL, VLongRepn, &value))
		VError ("Invalid index argument %s", str);
	return value;
}

static VBoolean Included (int v, VArgVector *vec)
/* returns TRUE if a particular index value is, according to the command
 * line, to be included in the output image.
 */
{
	int i, lb, ub;
	VStringConst str;

	if (vec->number == 0) return TRUE;
	for (i = 0; i < vec->number; i++) {
		ub = lb = Decode (((VStringConst *) vec->vector)[i]);
		if (i < vec->number - 2) {
			str = ((VStringConst *) vec->vector)[i + 1];
			if (strcmp (str, "to") == 0 || strcmp (str, "-") == 0 ||
				strcmp (str, ":") == 0) {
				ub = Decode (((VStringConst *) vec->vector)[i+2]);
				if (lb > ub)
					VError ("\"%s %s %s\" isn't a valid range",
					    	((VStringConst *) vec->vector)[i],
					    	((VStringConst *) vec->vector)[i+1],
					    	((VStringConst *) vec->vector)[i+2]);
				i += 2;
			}
		}
		if (v >= lb && v <= ub) return TRUE;
	}
	return FALSE;
}

static int convertParameter(struct param *b, char *base, int id)
/* convert a given parameter from a Bruker file regarding its description */
{
	char buf[256];
	FILE *fp;
	int i, status = NOTFOUND;

	/* check arguments */
	if (base == 0 || id == 0) return FATAL;

	/* construct filename and open file */
	if (b->file[0] == '%')
		sprintf(buf, "%s/%d/%s", base, id, b->file+3);
	else
		sprintf(buf, "%s/%s", base, b->file);
	fp = fopen(buf, "r");
	if (fp == 0)  {
		fprintf(stderr, "** brutov: cannot open %s\n", buf);
		return FATAL;
	};

	/* get parameter information from this file */
	while (fgets(buf, sizeof(buf), fp) && status != FOUND)  {
		char *s = strstr(buf, b->name);
		if (s == 0) continue;
		s += strlen(b->name);

		/* match following '=' to distinguish between similar
		 * names like 'GUI_slice_angle_1' and 'GUI_slice_angle_1_iso'
		 */
		if (*s++ != '=') continue;

		if (*s == '(')  {
			/* array argument follows */
			int size = atoi(s+1);
			if (size == 0)  {
				fprintf(stderr, "** brutov: illegal array size %d for parameter %s\n",
						size, b->name);
				fclose(fp);
				return FATAL;

			};
			/* now check the next line which contains the data */
			switch (b->type)  {
			case STRING:
				fgets(buf, sizeof(buf), fp);
				sscanf(buf+1, "%[^>]", (char *)b->dest);
				break;
			case STRING_ARRAY:
				{
				char hbuf[256*256];
				char **dp = (char **)b->dest;
				char *bp = hbuf;
				fread(hbuf, 1, sizeof(hbuf), fp);
				hbuf[sizeof(hbuf)-1] = 0;
				for (i = 0; i < 10; i++)  {
					int len = 0;
					sscanf(bp+1, "%*[^>]%n", &len);
					if (len == 0) {
						dp[i] = 0;
					} else {
						dp[i] = calloc(len+1, sizeof(char));
						sscanf(bp+1, "%[^>]", dp[i]);
						bp += len+1;
					}
					while (*bp != 0 && *bp != '<') bp++;
				}
				};
				break;
			case INT_ARRAY:
				{
				int *ip = (int *)b->dest;
				char *bp = buf;
				fgets(buf, sizeof(buf), fp);
				for (i = 0; i < size; i++)  {
					int offset, cnt;
next_int_line:
					cnt = sscanf(bp, "%d%n", &ip[i], &offset);
					if (cnt != 1) {
						if (fgets(buf, sizeof(buf), fp) == 0 ||  buf[0] == '#')  {
							fprintf(stderr, "** brutov: illegal contents \"%s\" for array parameter %s\n",
								buf, b->name);
							fclose(fp);
							return FATAL;
						} else {
							bp = buf;
							goto next_int_line;
						};
					};
					bp += offset;
				};
				};
				break;
			case DOUBLE_ARRAY:
				{
				double *dp = (double *)b->dest;
				char *bp = buf;
				fgets(buf, sizeof(buf), fp);
				for (i = 0; i < size; i++)  {
					int offset, cnt;
next_db_line:
					cnt = sscanf(bp, "%lf%n", &dp[i], &offset);
					if (cnt != 1) {
						if (fgets(buf, sizeof(buf), fp) == 0 ||  buf[0] == '#')  {
							fprintf(stderr, "** brutov: illegal contents \"%s\" for array parameter %s\n",
								buf, b->name);
							fclose(fp);
							return FATAL;
						} else {
							bp = buf;
							goto next_db_line;
						};
					};
					bp += offset;
				};
				};
				break;
			default:
				fprintf(stderr, "** brutov: illegal conversion for array parameter %s\n",
						b->name);
				fclose(fp);
				return FATAL;
			};
		} else {
			/* this is a simple argument */
			/* s points to the next parsable input */
			switch (b->type)  {
			case STRING:
			case ENUM:
				sscanf(s, "%[^\n]", (char *)b->dest);
				break;
			case DOUBLE:
				sscanf(s, "%lf", (double *)b->dest);
				break;
			case INT:
				sscanf(s, "%d", (int *)b->dest);
				break;
			default:
				fprintf(stderr, "** brutov: illegal conversion for simple parameter %s\n",
						b->name);
				fclose(fp);
				return FATAL;
			};
		};
		status = FOUND;
	};

	/* cleanup */
	fclose(fp);
	return status;
}

int analyzeParameters(char *base, int id)
/* read conversion parameters and prepare them according to Vista conventions */
{
	struct param *p;
	char fname[256];
	struct stat sb;
	int fs;

	fov[0] = 0; fov[1] = 0; fov[2] = 0; ang[0] = 0; ang[1] = 0; ang[2] = 0;
	loc = 0; gap = 0; thick = 0; nx = 0; ny = 0; nz = 0; ns = 0;
	nr = 0; pixsize = 4; dims[0] = dims[1] = dims[2] = 0; ips = 1; nips = 1;
	if (condition) VFree(condition); condition = 0;

	memset(pat, 0, sizeof(pat));
	memset(bday, 0, sizeof(bday));
	memset(date, 0, sizeof(date));
	memset(ori, 0, sizeof(ori));
	memset(sex, 0, sizeof(sex));
	memset(scan, 0, sizeof(scan));
	memset(exp, 0, sizeof(exp));
	memset(desc, 0, sizeof(desc));
	memset(sta, 0, sizeof(sta));
	dx = dy = dz = 0; nx = ny = nz = 0;

	/* MPIL_GEO supercedes MPIL supercedes Bruker parameters */
	/* better make mode explicit */
	if (convertParameter(&mode_table[1], base, id) == FOUND)  {
		/* use MPIL_GEO parameters */
		for (p = MPIL_GEO_table; p->file; p++)  {
			if (convertParameter(p, base, id) == FATAL) return 0;
		};
		mode = MPIL_GEO_MODE;
	} else if (convertParameter(&mode_table[0], base, id) == FOUND)  {
		/* use MPIL parameters */
		for (p = MPIL_table; p->file; p++)  {
			if (convertParameter(p, base, id) == FATAL) return 0;
		};
		mode = MPIL_MODE;
	} else {
		/* use Bruker parameters */
		for (p = Bruker_table; p->file; p++)  {
			if (convertParameter(p, base, id) == FATAL) return 0;
		};
		mode = NATIVE_MODE;
	};

	/* let's introduce some post-conversion heuristics */

	/* copy image dimensions from matrix dimensions */
	nx = dims[0]; ny = dims[1]; nz = dims[2];

	/* compute in-plane resolution from fov and matrix dimensions */
	dx = dims[0]? ((fov[0] * 10) / dims[0]): 0;
	dy = dims[1]? ((fov[1] * 10) / dims[1]): 0;
	dz = dims[2]? ((fov[2] * 10) / dims[2]): 0;

	/* if dz is not given, set it from gap (note that gap is in mm!) */
	if (dz == 0) dz = gap;

	/* if nz is not given, set it from ns */
	if (nz == 0) nz = ns;

	/* presence of nr denotes a functional dataset, so set nz from the timesteps given in nr */
	if (mode == MPIL_GEO_MODE && ips != 1)  {
		nz = nr;
		nips = abs(ips);
		fprintf(stderr, "** brutov: importing %d time-points in %d weightings of %d slices.\n", nr, nips, ns);
	} else if (nr > 1)  {
		nz = nr;
		fprintf(stderr, "** brutov: importing %d time-points of %d slices.\n", nr, ns);
	} else if (nr <= 1)  {
		nr = 1;
		ns = 1;
	};

	/* recode word type */
	if (strcmp(wtype, "_16BIT_SGN_INT") == 0)
		pixsize = 2;
	else if (strcmp(wtype, "_32BIT_SGN_INT") == 0)
		pixsize = 4;

	/* compute file size */
	fs = pixsize * ns * nz * ny * nx * nips;
	sprintf(fname, "%s/%d/pdata/1/2dseq", base, id);
	stat(fname, &sb);
	if (sb.st_size != fs)  {
		nz = sb.st_size / (pixsize * ny * nx);
		ns = nips;
		fprintf(stderr, "** brutov: parameters do not match data size, importing %d slices.\n", nz);
	};

	/* recode scan name */
	if (scan[0] != 0)  {
		if (strncmp(scan, "00_TRI", 6) == 0)
			strcpy(scan, "pilot");
		else if (strncmp(scan, "MDEFT", 5) == 0)
			strcpy(scan, "T1");
		else if (strncmp(scan, "IR_RARE", 6) == 0)
			strcpy(scan, "IR");
		else if (strncmp(scan, "T2_", 3) == 0)
			strcpy(scan, "T2");
	};

 	return 1;
}

static VImage convertData(FILE *fp, int sno, int ino, int id, double white, double black, int fold)
/* given parameters, convert a Bruker file to Vista format */
{
	int i, x, y, z, slice_size, v, flip, of;
	char attr[256], buf[64];
	unsigned char *sp, *slice;
	VImage dst, tmp;
        VRepnKind repn;
        double pos = 0;
	unsigned long offset;

	/* now, a Bruker location is given for the center of the slice stack */
	/* modify it for the first slice in the image stack */
	if (nr > 1)  {
		/* for a functional dataset */
		pos = loc - ((ns-1) * gap)/2 + sno * gap;
	} else {
		/* for an anatomical dataset */
		pos = loc - ((nz-1) * gap)/2;
	};

	repn = (pixsize == 4)? VLongRepn: VShortRepn;
	slice_size = nx * ny * pixsize;
	slice = VMalloc(slice_size);
	tmp = VCreateImage(nz, ny, nx, repn);

	/* recode transversal direction into axial direction */
	flip = FALSE;
	if (ori[0] == '\0' || strcmp(ori, "sagittal") == 0)
		flip = FALSE;
	else if (strcmp(ori, "transversal") == 0)  {
		strcpy(ori, "axial");
		flip = TRUE;
	} else if (strcmp(ori, "coronal") == 0 || strcmp(ori, "axial") == 0)  {
		flip = TRUE;
	};

	/* read, swap and flip image */
	for (z = 0; z < nz; z++)  {
		of = z * ns * nips;
		of += (ips < 0)? sno * nips + ino: ino * ns + sno;
		offset = of * slice_size;
		fseek(fp, offset, 0);
		if (fread(slice, slice_size, 1, fp) < 1)  {
			fprintf(stderr, "** brutov: data file incomplete.\n");
			VDestroyImage(tmp);
			VFree(slice);
			return 0;
		};
		sp = (unsigned char *)slice;
		for (y = 0; y < ny; y++)  {
			for (x = 0; x < nx; x++, sp += pixsize)  {
                                if (pixsize == 4)  {
	        			v = (sp[3]) | (sp[2] << 8) | (sp[1] << 16) | (sp[0] << 24);
				} else {
	        			v = (sp[1]) | (sp[0] << 8);
				};
				if (flip == TRUE)
                                	VSetPixel(tmp, z, y, nx-x-1, v);
                                else
                                	VSetPixel(tmp, z, y, x, v);
			};
		};
	};
	VFree(slice);

	if ((mode == MPIL_GEO_MODE && ips != 1) || (nr > 1)) {
		dst = tmp;
	} else {
		/* scale intensity */
		dst = VScaleIntensity(tmp, white, black, fold);
		VDestroyImage(tmp);
	};

	/* finally, copy attributes to Vista file */
	VSetAttr(VImageAttrList(dst), "patient", NULL, VStringRepn, pat);
	VSetAttr(VImageAttrList(dst), "date", NULL, VStringRepn, date);
	sprintf(attr, "MR %s", method);
	VSetAttr(VImageAttrList(dst), "modality", NULL, VStringRepn, attr);
	sprintf(attr, "%G %G %G", ang[0], ang[1], ang[2]);
	VSetAttr(VImageAttrList(dst), "angle", NULL, VStringRepn, attr);
	sprintf(attr, "%G %G %G", dx, dy, dz);
	VSetAttr(VImageAttrList(dst), "voxel", NULL, VStringRepn, attr);
	sprintf(attr, "%G", pos);
	VSetAttr(VImageAttrList(dst), "location", NULL, VStringRepn, attr);

	if (condition) {
		VSetAttr(VImageAttrList(dst), "condition", NULL,
			VStringRepn, condition);
		if (mode == MPIL_GEO_MODE && exp[0])  {
			if (nips > 1)
				sprintf(attr, "MR %02d-%02d-%02d %s", id, sno, ino, exp);
			else
				sprintf(attr, "MR %02d-%02d %s", id, sno, exp);
		} else
			sprintf(attr, "MR %02d-%02d func", id, sno);
		VSetAttr(VImageAttrList(dst), "name", NULL, VStringRepn, attr);
		VSetAttr(VImageAttrList(dst), "slice_time", NULL, VDoubleRepn, sta[sno]);
	} else  {
		sprintf(attr, "MR %02d %s %s", id, scan, ori);
		VSetAttr(VImageAttrList(dst), "name", NULL, VStringRepn, attr);
	};
	if (*ori)
		VSetAttr(VImageAttrList(dst), "orientation", NULL, VStringRepn, ori);
	if (*bday)
		VSetAttr(VImageAttrList(dst), "birth", NULL, VStringRepn, bday);
	if (*sex)
		VSetAttr(VImageAttrList(dst), "sex", NULL, VStringRepn, sex);
	for (i = 0; i < 10; i++)  {
		if (desc[i] == 0) continue;
		sprintf(buf, "MPIL_vista_%d", i);
		VSetAttr(VImageAttrList(dst), buf, NULL, VStringRepn, desc[i]);
	};
	VSetAttr(VImageAttrList(dst), "convention", NULL, VStringRepn, "natural");
	return dst;
}

static int analyzeData(VImage *images, int *pos, char *base, int id, double white, double black, int fold)
{
	FILE *fp;
	int sno, ino, i;
	char fname[256];
	struct stat sb;

	sprintf(fname, "%s/%d/pdata/1/2dseq", base, id);
	stat(fname, &sb);
	fp = fopen(fname, "r");
	if (fp == 0)  {
        	fprintf(stderr, "** brutov: data file %s is not accessible.\n", fname);
        	return 0;
        };

	/* check for functional datasets */
	if (nr > 1)  {
		if (mode == MPIL_GEO_MODE && exp[0])  {
               		fprintf(stderr, "** brutov: flagged as %s dataset.\n", exp);
		} else {
               		fprintf(stderr, "** brutov: guessing a functional dataset.\n");
		};
               	if (condition == 0)  {
               		/* build a condition argument */
               		condition = VMalloc(nr+1);
               		memset(condition, '1', nr);
               		condition[nr] = 0;
               	};
	};

	for (sno = 0; sno < ns; sno++)  {
		for (ino = 0; ino < nips; ino++)  {
			VImage im = convertData(fp, sno, ino, id, white, black, fold);
			if (im)  {
				images[*pos] = im;
				(*pos)++;
			} else  {
				fclose(fp);
				return 0;
			};
		};
	};
	fclose(fp);
	return 1;
}

static int convertFile(VImage *images, int *pos, char *base, int id,
	double white, double black, int fold)
{
	if (analyzeParameters(base, id) == 0)  {
		fprintf(stderr, "** brutov: illegal parameters in path %s.\n", base);
		return 0;
	};
	if (analyzeData(images, pos, base, id, white, black, fold) == 0)  {
		fprintf(stderr, "** brutov: illegal data in path %s.\n", base);
		return 0;
	};
	return 1;
}

static int convertSession(VImage *images, char *base,
	double white, double black, int fold, VArgVector *vec)
{
	char filename[512];
	int id, n = 0;

	for (id = 1; id < 99; id++)  {
		if (vec && Included(id, vec) == FALSE) continue;
		sprintf(filename, "%s/%d", base, id);
		if (access(filename, R_OK) < 0) continue;
		fprintf(stderr, "** brutov: converting %s\n", filename);
		convertFile(images, &n, base, id, white, black, fold);
	};
	return n;
}

static int unpackTar(char *fname, char *fpath)
{
	char path[1024], dname[1024], tarfile[1024], cmd[1024];
	DIR *dir;
	struct dirent *entry;
	int found;

	/* check were we are */
	getcwd(path, sizeof(path));

	/* generate full pathname to input file */
	if (fname[0] != '/')
		sprintf(tarfile, "%s/%s", path, fname);
	else
		strcpy(tarfile, fname);

	/* create well-known directory */
	sprintf(dname, "%s/.brutov", path);
	if (mkdir(dname, 0777) < 0 && errno != 17)  {
		fprintf(stderr, "** brutov: cannot create temporary directory %s.\n", dname);
		return 0;
	};

	/* unpack tar file in this directory */
	chdir(dname);
	sprintf(cmd, "tar xf %s", tarfile);
	fprintf(stderr, "** brutov: unpacking %s.\n", fname);
	if (system(cmd) < 0)  {
		fprintf(stderr, "** brutov: tar error %d.\n", errno);
		goto cleanup;
	};

	/* generate path to unpack directory */
	dir = opendir(dname);
  	if (dir == 0) {
  		/* should not happen */
		sprintf(cmd, "rm -rf %s", dname);
		goto cleanup;
	};
	found = 0;
	while ((entry = readdir(dir)) != 0)  {
		if (strncmp(entry->d_name, fname, 2) == 0)  {
  			sprintf(fpath, "%s/%s", dname, entry->d_name);
  			found = 1;
  			break;
  		};
  	};
  	closedir(dir);
  	if (found == 0) goto cleanup;

	/* go back to our starting directory */
	chdir(path);
  	return 1;

cleanup:
	sprintf(cmd, "rm -rf %s", dname);
	system(cmd);
	return 0;
}

/*
 *  main
 *
 *  Program entry point.
 */

int main (int argc, char **argv)
{
	static VStringConst in_file, out_file;
        static VBoolean in_found, obj_found, out_found = 0;
	static VDouble white = 0.5;
	static VDouble black = 10;
	static VLong fold = 0;
	static VArgVector objects;
	static VOptionDescRec options[] = {
		{ "in", VStringRepn, 1, &in_file, &in_found, NULL,
			"Tar file or directory containing source files" },
		{ "out", VStringRepn, 1, &out_file, &out_found, NULL,
			"File to contain created image" },
		{ "white", VDoubleRepn, 1, &white, 0, NULL,
			"Percent of histogram border voxels mapped to white" },
		{ "black", VDoubleRepn, 1, &black, 0, NULL,
			"Percent of histogram border voxels mapped to black" },
		{ "fold", VLongRepn, 1, &fold, 0, NULL,
			"Fold slabs at slice" },
		{ "ds", VStringRepn, 0, &objects, &obj_found, NULL,
	  		"Datasets to be selected" },
		{ "rep", VLongRepn, 1, &rep, 0, NULL,
			"Images per stimulation condition" }
        };
	char path[1024], cmd[1024];
        VImage images[1000];
	struct stat sb;
        int cnt = 0, dirflag = 0, status = 0;

	if (! VParseCommand(VNumber (options), options, &argc, argv) ||
        	! VIdentifyFiles(VNumber (options), options, "out", &argc, argv, 1))
        	goto Usage;
        if (argc > 1) {
                VReportBadArgs(argc, argv);
Usage:
                VReportUsage(argv[0], VNumber(options), options, 0);
                return 1;
        };

        /* see what we have got as an input file */

        /* stdin is illegal */
        if (strcmp(in_file, "-") == 0) {
		fprintf(stderr, "** brutov: illegal to use stdin for input.\n");
                return 1;
        };

        /* check for a tar file */
        if (strstr(in_file, ".tar") || strstr(in_file, ".TAR")) {
                dirflag = unpackTar((char *)in_file, path);
                if (dirflag == 0) return 1;
                in_file = path;
        } else {
        	/* check for a directory */
		if (stat(in_file, &sb) < 0) {
			fprintf(stderr, "** brutov: cannot access %s.\n", in_file);
                	return 1;
                };
                if (S_ISDIR(sb.st_mode) == 0)  {
			fprintf(stderr, "** brutov: %s is not a directory.\n", in_file);
                	return 1;
                };
	};
 	cnt = convertSession(images, (char *)in_file, white, black, fold, &objects);
	if (cnt) {
		FILE *fp;
		fp = VOpenOutputFile(out_file, TRUE);
		status = VWriteImages(fp, 0, cnt, images);
		fclose(fp);
	};
	if (dirflag)  {
		getcwd(path, sizeof(path));
                sprintf(cmd, "rm -rf %s/.brutov", path);
                system(cmd);
	};
	if (status)  {
		fprintf (stderr, "%s: Converted %d image(s) from Bruker to Vista format.\n",
			argv[0], cnt);
	};
	return (status == 0);
}
