.ds Vn 1.12
.TH brutov 1Vi "02 Januar 1994" "Vista Version \*(Vn" .SH NAME
brutov \- convert Bruker data format to Vista data file
.SH SYNOPSIS
\fBbrutov\fR [\fB-\fIoption\fR ...] [\fIinfile\fR] [\fIoutfile\fR]
.SH DESCRIPTION
\fBbrutov\fP converts a group of Bruker data files into a Vista data file.
The following attributes in the output file are extracted from
the input file:
.RS 2n
.IP \(bu
patient		patient name
.IP \(bu
date		date of study
.IP \(bu
modality		"MR" sequence information
.IP \(bu
params		parameters of sequence
.IP \(bu
voxel		real world dimensions of a voxel in mm ("1.0 1.0 1.0")
.IP \(bu
location	real world position of first slice in dataset ("12.0")
.LP
.RE
.SH "COMMAND LINE OPTIONS"
\fBbrutov\fP accepts the following options:
.IP \fB-help\fP 15n
Prints a message describing options.
.IP "\fB-in\fP \fIinfile\fP
Specifies a directory or a tar file that contains the source data.
.IP "\fB-out\fP \fIoutfile\fP"
Specifies the output file, which will be a Vista data file.
.IP "\fB-fold\fP \fInumber\fP"
If the slices are folded in z-direction, specify top slice number here.
.IP "\fB-white\fP \fInumber\fP"
Specifies the percentage of voxels that will be mapped to white (default 0.5%).
.IP "\fB-black\fP \fInumber\fP"
Specifies the percentage of voxels that will be mapped to black (default 10%).
.IP "\fB-ds\fP \fI1 2\fP ..." 15n
Select datasets \fI1\fP, \fI2\fP, etc. from an experiment.
.IP "\fB-rep\fP \fInumber\fP"
Specifies the repition count in a functional stimulation (default 4).
.LP
The keyword ``\fBto\fP'' can be used to specify a range of indices, as in
``\fB-ds\ \fI1\fP\ \fBto\fP\ \fI2\fP''.
.LP
Axial and coronal slices are flipped from the radiologic convention into
the natural convention, i.e. the left image side corresponds to the left
body side. An attribute "convention: natural" is appended to the list
to document this orientation. Note that sagittal slices are not flipped,
i.e. they are found "nose left".

.SH NOTES
The following command line
.IP
brutov -in myfile.tar -out vista-file.v -white 3
.LP
expects myfile.tar and converts them into a Vista file. After
conversion, a histogram equalisation is performed, in which 3% of the voxel
on the right side of the histogram are mapped to white.
.PP
The following command line
.IP
brutov -in WA1T961101.GB1 -out vista-file.v -rep 4
.LP
expects a directory tree with root WA1T961101.GB1 containing Bruker parameter
and data files. When a functional dataset is found, and no
experimental conditions are present in the datasets, stimulation is
assumed to be off in the first 4 images, then on in the following four images.
.PP
To select only a subset of the datasets in an experiment, use
.IP
brutov -in WA1T961101.GB1 -out vista-file.v -ds 4 5 7 to 14
.LP
This will convert datasets 4, 5, and 7 to 14 only.
.PP
Multislice fMRI datasets are converted into a series of single-slice Vista
datasets. Each functional slice is identified by a 2-digit sequence number
and a 2-digit slice number. Slice numbers are zero-based, i.e. 11-02 identifies
the third slice of dataset 11.

.SH "SEE ALSO"
.BR VImage (3Vi),
.BR Vfile (5Vi),
.BR Vista (7Vi),
.SH AUTHOR
Frithjof Kruggel <kruggel@cns.mpg.de>
