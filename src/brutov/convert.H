/******************************************************************************/
/*                                                                            */
/* Copyright (C) 1998-2002 by Thomas Arnold                                   */
/*                                                                            */
/* Max-Planck-Institute of Cognitive Neuroscience                             */
/* PO Box 500 355, D-04303 Leipzig, Germany                                   */
/*                                                                            */
/* Phone +49 341 9940-131                                                     */
/* Fax   +49 341 9940-204                                                     */
/*                                                                            */
/* Email arnold@cns.mpg.de                                                    */
/* WWW   www.cns.mpg.de                                                       */
/*                                                                            */
/* This software may be freely copied, modified, and redistributed provided   */
/* that this copyright notice is preserved on all copies.                     */
/*                                                                            */
/* You may not distribute this software, in whole or in part, as part of any  */
/* commercial product without the express consent of the author.              */
/*                                                                            */
/* There is no warranty or other guarantee of the fitness of this software    */
/* for any purpose. It is provided solely "as is".                            */
/*                                                                            */
/******************************************************************************/


/*******************************************************************************

RCS

$Source$
$Revision: 404 $
$Date: 2002-09-24 15:56:19 +0200 (Tue, 24 Sep 2002) $

$Locker$

$Log$
Revision 1.1  2002/09/24 13:56:19  arnold
Initial revision


*******************************************************************************/


#ifndef CONVERT_H_INCLUDED
#define CONVERT_H_INCLUDED

extern "C"
{
   #include <Vlib.h>
   #include <VImage.h>
   #include <option.h>
   #include <mu.h>
}


/*------------------------------------------------------------------------------

ConvertType
===========

Src    source image with voxels of type T
Dest   converted image with voxels of type U (created)
Type   type of created image (MUST correspond to type U)

------------------------------------------------------------------------------*/

template <class T, class U> void ConvertType (VImage Src, VImage& Dest, VRepnKind Type)
{
   int Voxels;   /* number of voxels */

   T* src;    /* source data pointer      */
   U* dest;   /* destination data pointer */

   long n;   /* index */


   /* get source image size */
   Voxels = VImageNPixels (Src);

   /* create converted image */
   Dest = VCreateImage (VImageNBands (Src), VImageNRows (Src), VImageNColumns (Src), Type);
   VImageAttrList (Dest) = VCopyAttrList (VImageAttrList (Src));


   /* convert image */
   src  = (T*) VPixelPtr (Src,  0, 0, 0);
   dest = (U*) VPixelPtr (Dest, 0, 0, 0);
   for (n = 0; n < Voxels; n++)
      *(dest++) = (U) *(src++);

} /* ConvertType */

template <class T, class U> void ConvertType (VImage& Src, VRepnKind Type)
{
   VImage Dest;   /* destination image */


   /* convert image */
   ConvertType<T,U> (Src, Dest, Type);
   VDestroyImage (Src);
   Src = Dest;

} /* ConvertType */

/*----------------------------------------------------------------------------*/

#endif
