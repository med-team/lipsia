# dictov
#
# CMakeLists.txt
# Author: Thomas Proeger

PROJECT(dictov)

# project specific variables
SET (SOURCES 
  NormVals.c
  dictov.C
)

SET(DCMTK_LIBS
  ${DCMTK_DCMDATA_LIBRARY}
  ${DCMTK_OFSTD_LIBRARY}
  pthread)

SET(LIBS
  lipsia					# should be done by a FindPackage script
  m
  ${ZLIB_LIBRARIES}
  ${GSL_GSL_LIBRARY}
  ${DCMTK_LIBS}
  ${VIA_LIBRARY}
  ${VIAIO_LIBRARY})

ADD_EXECUTABLE(dictov ${SOURCES})

SET_TARGET_PROPERTIES(dictov
    PROPERTIES COMPILE_FLAGS -ansi
    LINK_FLAGS -Wl)

TARGET_LINK_LIBRARIES(dictov ${LIBS})

INSTALL(TARGETS dictov
        RUNTIME DESTINATION ${LIPSIA_INSTALL_BIN_DIR}
        COMPONENT RuntimeLibraries)
