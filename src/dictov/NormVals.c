/****************************************************************
 *
 * vpreprocess: NormVals.c
 *
 * Copyright (C) Max Planck Institute
 * for Human Cognitive and Brain Sciences, Leipzig
 *
 * <lipsia@cbs.mpg.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * $Id: NormVals.c 3181 2008-04-01 15:19:44Z karstenm $
 *
 *****************************************************************/

#include <VImage.h>
#include <Vlib.h>
#include <mu.h>
#include <option.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

void
VNormVals(VAttrList list,VLong fscale)
{
  VImage src;
  VAttrListPosn posn;
  VLong u, npix=0, outl=0;
  int r,c,j;
  double perc;



  /* Go through all objects */
  for (VFirstAttr (list, & posn); VAttrExists (& posn); VNextAttr (& posn)) {
    if (VGetAttrRepn (& posn) != VImageRepn) continue;
    VGetAttrValue (& posn, NULL,VImageRepn, & src);
    if (VPixelRepn(src) != VShortRepn) continue;

    /* Go through the pixels */
    npix += VImageNPixels(src);
    for (r=0; r<VImageNRows(src); r++) {
      for (c=0; c<VImageNColumns(src); c++) {
	for (j=0; j<VImageNBands(src); j++) {
	  u = (VLong)VPixel(src,j,r,c,VShort);
	  u *= fscale;
	  if (u>32766) {
	    u=32766;
	    outl++;
	  }
	  if (u<-32766) {
	    u=-32766;
	    outl++;
	  }
	  VPixel(src,j,r,c,VShort) = (VShort)u;
	}
      }
    }
  }


  /* Warnings and errors */
  perc = (double)outl * 100.0/(double)npix;
  if (perc > 0.1)
    fprintf(stderr,"Warning: %1.2f percent outliers due to scaling of functional data\n",perc);
  if (perc > 2)
    VError(" Reduce '-fscale' parameter");

}
