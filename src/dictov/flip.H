/****************************************************************
 *
 * vdictov: flip.H
 *
 * Copyright (C) Max Planck Institute 
 * for Human Cognitive and Brain Sciences, Leipzig
 *
 * Author Thomas Arnold, 2002, <lipsia@cbs.mpg.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 * $Id: flip.H 687 2004-02-12 09:29:58Z karstenm $
 *
 *****************************************************************/


#ifndef FLIP_H_INCLUDED
#define FLIP_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <float.h>
#include <math.h>
#include <time.h>
#include <assert.h>

extern "C"
{
   #include <Vlib.h>
   #include <VImage.h>
   #include <option.h>
   #include <mu.h>
}


/*------------------------------------------------------------------------------

Flip
====

Src    source image with voxels of type T
Dir    direction of flip (XFlip = 0, YFlip = 1 and ZFlip = 2)
Dest   flipped image with voxels of type T (created)

------------------------------------------------------------------------------*/

const int XFlip = 0;
const int YFlip = 1;
const int ZFlip = 2;

template <class T> void Flip (VImage Src, int Dir, VImage& Dest)
{
   int Bands;     /* number of bands   */
   int Rows;      /* number of rows    */
   int Columns;   /* number of columns */

   T* src;    /* source data pointer      */
   T* dest;   /* destination data pointer */

   int  stride = 0;              /* stride     */
   int  I = 0,  J = 0,  K = 0;   /* sizes      */
   int  i = 0,  j = 0,  k = 0;   /* indices    */
   int *x = 0, *y = 0, *z = 0;   /* references */


   /* get source image size */
   Bands   = VImageNBands   (Src);
   Rows    = VImageNRows    (Src);
   Columns = VImageNColumns (Src);

   /* create destination image */
   Dest = VCreateImage (Bands, Rows, Columns, VPixelRepn (Src));
   VImageAttrList (Dest) = VCopyAttrList (VImageAttrList (Src));


   /* set flip parameters */
   if (Dir == XFlip) {K = Bands;   z = &k; J = Rows;    y = &j; I = Columns; x = &i; stride = 1;             }
   if (Dir == YFlip) {K = Columns; x = &k; J = Bands;   z = &j; I = Rows;    y = &i; stride = Columns;       }
   if (Dir == ZFlip) {K = Rows;    y = &k; J = Columns; x = &j; I = Bands;   z = &i; stride = Rows * Columns;}

   /* flip image */
   for (k = 0; k < K; ++k)
      for (j = 0; j < J; ++j)
      {
         /* setup flip line */
         i    = 0;
         src  = (T*) VPixelPtr (Src, *z, *y, *x);
         i    = I - 1;
         dest = (T*) VPixelPtr (Dest, *z, *y, *x);

         /* flip entries */
         for (i = 0; i < I; ++i)
         {
            *dest = *src;
            src  += stride;
            dest -= stride;
         }
      }

} /* Flip */

template <class T> void Flip (VImage& Src, int Dir)
{
   VImage Dest;   /* destination image */


   /* flip image */
   Flip<T> (Src, Dir, Dest);
   VDestroyImage (Src);
   Src = Dest;

} /* Flip */

/*----------------------------------------------------------------------------*/

#endif
