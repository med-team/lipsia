PROJECT(LIBARFIT)

# configure source files
SET(LIBARFIT_SRC
   arfit.c arfit_schloegl.c arfit_via.c arfit_error.c  arfit_schneider.c
   gsl_matrix_wrapper.c)

ADD_LIBRARY(arfit ${LIBARFIT_SRC})
TARGET_LINK_LIBRARIES(arfit m ${GSL_GSL_LIBRARY} ${VIAIO_LIBRARY})

SET_TARGET_PROPERTIES(arfit
    PROPERTIES COMPILE_FLAGS -ansi
    LINK_FLAGS -Wl)

# only install when built as a shared lib
IF (BUILD_SHARED_LIBS)
  INSTALL(TARGETS arfit
    LIBRARY DESTINATION ${LIPSIA_INSTALL_LIB_DIR} COMPONENT RuntimeLibraries)
ENDIF (BUILD_SHARED_LIBS)
