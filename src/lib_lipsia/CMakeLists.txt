PROJECT(LIBLIPSIA)

# configure source files
SET(LIBLIPSIA_SRC
    GeoInfo.c StatsConversions.c Talairach.c gsl_futils.c gsl_dutils.c via2gsl.c
    SmoothnessEstim.c wilcoxtables.c wilcox_pvals.c Convolve.c GaussConv.c
    GetVersion.c)

ADD_LIBRARY(lipsia ${LIBLIPSIA_SRC})
TARGET_LINK_LIBRARIES(lipsia m ${VIAIO_LIBRARY} ${GSL_GSL_LIBRARY})

SET_TARGET_PROPERTIES(lipsia
    PROPERTIES COMPILE_FLAGS -ansi
    LINK_FLAGS -Wl)

# only install when built as a shared lib
IF (BUILD_SHARED_LIBS)
  INSTALL(TARGETS lipsia
    LIBRARY DESTINATION ${LIPSIA_INSTALL_LIB_DIR} COMPONENT RuntimeLibraries)
ENDIF (BUILD_SHARED_LIBS)
