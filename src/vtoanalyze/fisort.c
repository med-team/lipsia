/****************************************************************
 *
 * fisort.c:  utility functions for vista functional dataset handling
 * 
 * MPI of Cognitive Neuroscience, Leipzig
 *
 * Originally by  R. Nowagk,   Jun. 1998
 * Modified   by  Till D�rges, Jan. 2001, <doerges@cns.mpg.de>
 *
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges -- 01/11/2001 -- started documenting/hacking the code
 *  Till D�rges -- 01/16/2001 -- disabled isfunctimg() in scanlist_init()
 *  Till D�rges -- 01/17/2001 -- well, this was a mistake...
 *
 *  Till D�rges -- 06/03/2001 -- restructured code,
 *   support for compressed images
 *
 *  Till D�rges -- 14/03/2001 -- added hack for being not strict
 *    when identifying functional images
 *
 *****************************************************************/

#include <stdlib.h>

#include <viaio/mu.h>
#include <viaio/VImage.h>

#include "fisort.h"
#include "tools.h"
#include "imagetools.h"


/* scanlist ---------------------------------------------- */

/* Looks up ids (=sequence-no.) in name-attribute of an image. */
void get_scan_slice(VImage image, int *scan, int *slice) {
  VStringConst name;
  int sres;

  if (VGetAttr(VImageAttrList(image), "name", 0, VStringRepn, &name) != VAttrFound) {
    VError("%s: cannot obtain name-header", __FUNCTION__);
  } else {
    sres=sscanf(name, "%*s%d%d", scan, slice);
    if (sres != 2)
      VError("%s: cannot obtain scan id/slice", __FUNCTION__);
  }
  *slice = - (*slice);   /* there's a '-' between scan-numer and id */
} /* get_scan_slice */


/*
   Insert image (a functional object) into scanv.
   posn is needed to continue searching the attribute list from which
   image was extracted in case image is compressed.

   strict defines, whether to be picky about identifying functional
   objects. Normally this should be true, as this allows for several
   scan-series in one file. If it is false, it's assumed that every
   functional object belongs to the same scan (there can only be one)
   and that they're all in consecutive order. 
*/
void register_scan_slice(scanlist_t **scanv, int *scanc,
			 VImage image, VAttrListPosn posn,
			 VBoolean strict) {
  int s, f = -1;
  int scan, slice;
  scanlist_t *sv = *scanv;
  VImage compressedimage=NULL;

  /* There can be only one scan, when not strict. So for each object,
     it's simply incremented. */
  static int nonstrictnslice=0;


  if (iscompressed(image)) {
    compressedimage=image;
    image=decompress(compressedimage, posn, 0, 0, 0);
    if(!image)
      VError("%s: Couldn't decompress image", __FUNCTION__);
    posn.ptr->value=image; /* hook decompressed image into attr-list */
    VDestroyImage(compressedimage); compressedimage=NULL;
  }

  /* get ids (sequence-no. of functional objects) */
  if(strict)
    get_scan_slice(image, &scan, &slice);
  else {
    slice=nonstrictnslice;
    scan=0;
    nonstrictnslice++;
  }

  /* scan present in list ? */
  for (s = 0; s < *scanc && f == -1; s++)
    if (sv[s].id == scan) f = s;
    
  /* present scan - count up and register slices */
  if (f >= 0) {
    if (sv[f].nslices % 16 == 0)
      sv[f].slices = (VImage *) 
        VRealloc(sv[f].slices, (sv[f].nslices + 16) * sizeof(VImage));
    sv[f].slices[sv[f].nslices] = image;
    sv[f].nslices++;
    if (VImageNBands(image) != sv[f].nframes)
      VError("%s: nframes mismatch", __FUNCTION__);
  }
  
  /* new scan - add a list entry */
  else {
    if (*scanc % 16 == 0)
      *scanv = (scanlist_t *) VRealloc(*scanv, (*scanc + 16) * sizeof(scanlist_t));
    (*scanv)[*scanc].id = scan;
    (*scanv)[*scanc].nslices = 1;
    (*scanv)[*scanc].nframes = VImageNBands(image);
    (*scanv)[*scanc].nrows = VImageNRows(image);
    (*scanv)[*scanc].ncolumns = VImageNColumns(image);
    (*scanv)[*scanc].repn = VPixelRepn(image);
    (*scanv)[*scanc].slices = (VImage *) VMalloc(16 * sizeof(VImage));
    (*scanv)[*scanc].slices[0] = image;

    (*scanc)++;
  }
} /* register_scan_slice() */


scanlist_t *scanlist_init(VAttrList list, int *scanc, int strict) {
  VAttrListPosn posn;
  VImage image;

  scanlist_t *scanv = NULL;
  *scanc = 0;

  for (VFirstAttr (list, & posn); VAttrExists (& posn); VNextAttr (& posn)) {
    /* skip non-functional object */
    if (VGetAttrRepn(&posn) != VImageRepn) continue;
    if (!(VGetAttrValue(&posn, NULL, VImageRepn, &image))) {
      VWarning("%s: Couldn't extract image. Skipping", __FUNCTION__);
    }
    if (! image) continue;
    if (strict) {
      if (! isfunctimgstrict(image)) continue;
    } else {
      if (! isfunctimg(image)) continue;
    }

    /* register functional object */
    register_scan_slice(&scanv, scanc, image, posn, strict);
  }

  return scanv;
} /* scanlist_init() */


void scanlist_free(scanlist_t *scanv, int scanc) {
  int i;

  for (i = 0; i < scanc; i++) {
    VFree(scanv[i].slices);
  }
  VFree(scanv);
} /* scanlist_free() */


VImage  scanlist_get_volume (scanlist_t *scanv, int scan, int frame) {
  VBand *src_bands;
  int   i;
  VImage dst;
  scanlist_t *s = &scanv[scan];
  
  if (frame >= s->nframes)
    VError("%s: illegal frame", __FUNCTION__);
  
  /* ok - rearrange the sliced data to volumes */
  src_bands = (VBand *) VMalloc(s->nslices * sizeof(VBand));
  for (i = 0; i < s->nslices; i++)
    src_bands[i] = frame;
  dst = VCombineBands(s->nslices, s->slices, src_bands, NULL);
  VFree(src_bands);
  
  /* to preserve attributes for functional image */
  VCopyImageAttrs(s->slices[0], dst);
  
  return dst;
} /* scanlist_get_volume() */
