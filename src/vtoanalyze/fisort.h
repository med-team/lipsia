/****************************************************************
 *
 * fisort.h: utility functions for vista functional dataset handling
 *
 * MPI of Cognitive Neuroscience, Leipzig
 *
 * Originally by  R. Nowagk,   Jun. 1998
 * Modified   by  Till D�rges, Jan. 2001, <doerges@cns.mpg.de>
 *
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges -- 01/11/2001 -- started documenting/hacking the code
 *  Till D�rges -- 06/03/2001 -- restructured code
 *
 *  Till D�rges -- 14/03/2001 -- added hack for being not strict
 *    when identifying functional images
 *
 *****************************************************************/

#ifndef FISORT_H
#define FISORT_H

#include <viaio/VImage.h>


/* scanlist -------------------------------------------------- */
typedef struct {                       /* input functional data layout  */
  int        id;
  int        nslices;/* img-objs found in serie (= nbands per timestep) */
  VImage    *slices;
  int        nframes, nrows, ncolumns;            /* frames = timesteps */
  VRepnKind  repn;
  float      voxel[3];
} scanlist_t;


/* 
  Determine scan/frame layout of a functional dataset.

  A scan is equiv. to a serie. One file can contain serveral series.
  slice_list is searched for the parts of of each functional
  serie/scan.

  Usually, the objects belonging to one serie can be identified by
  their name-tag, which contains the no. of the scan and the
  sequence-no. (=id) of the object in question.

  However, strict determines, whether to be picky when id. a part of
  the scan. Usually it should be true. false should only be set if
  there's only one scan in the file and all objects of this scan are
  in consecutive order.

  returns:
    allocated list of scan descriptions (caller must free)
    number of scans in scanc */
scanlist_t *scanlist_init(VAttrList slice_list, int *scanc, int strict);

void scanlist_free(scanlist_t *scanv, int scanc);

/* 
  To access input funct. slices as a sequence of volumes/images.
    return: allocated image (caller must free!)
*/
VImage scanlist_get_volume    (scanlist_t *scanv, int scan, int frame);

#endif /* FISORT_H */
