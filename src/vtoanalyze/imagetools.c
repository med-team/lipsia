/****************************************************************
 *
 * imagetools.c: useful stuff for vista images
 *
 * MPI of Cognitive Neuroscience, Leipzig
 *
 * Till D�rges, March 2001, <doerges@cns.mpg.de>
 *
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges -- 06/03/2001 -- created (restructured code)
 *
 *  Till D�rges -- 14/03/2001 -- added hack for being not strict
 *    when identifying functional images
 *
 *****************************************************************/

#include "imagetools.h"
#include "vtoanalyze.h"


int isfunctimgstrict(VImage image) {
  VBoolean gotname;
  VStringConst name;
  int scan, slice;

  gotname=(   (VGetAttr(VImageAttrList(image), "name", 0, VStringRepn, &name) == VAttrFound)
	   && (sscanf(name, "%*s%d%d", &scan, &slice) == 2)  );
  return (gotname && (VShortRepn == VPixelRepn(image)));
} /* isfunctimgstrict() */


int isfunctimg(VImage image) {
  VString str;
  VLong tr;

  if ((VGetAttr(VImageAttrList(image),"MPIL_vista_0",NULL,VStringRepn,(VPointer)&str)!=VAttrFound)
      && (VGetAttr(VImageAttrList(image),"repetition_time",NULL,VLongRepn,&tr)!=VAttrFound))
    return FALSE;

  return (VShortRepn == VPixelRepn(image));
} /* isfunctimg() */


int isanaimg(VImage image) {
  VString str;
  VLong tr;

  if ((VGetAttr(VImageAttrList(image),"MPIL_vista_0",NULL,VStringRepn,(VPointer)&str)==VAttrFound)
      || (VGetAttr(VImageAttrList(image),"repetition_time",NULL,VLongRepn,&tr)==VAttrFound))
    return FALSE;

  return (VUByteRepn == VPixelRepn(image) || VShortRepn == VPixelRepn(image));
} /* isanaimg() */


int iszmap(VImage image) {
  return (VFloatRepn == VPixelRepn(image));
} /* iszmap() */


int iscompressed(VImage image) {
  VAttrListPosn posn;
  int ncols=VImageNColumns(image);
  int nrows=VImageNRows(image);
  VBoolean orig_nrows_present, orig_ncols_present;

  orig_nrows_present=VLookupAttr(VImageAttrList(image), V_HDR_ORI_NROWS, &posn);
  orig_ncols_present=VLookupAttr(VImageAttrList(image), V_HDR_ORI_NCOLS, &posn);

  return ((ncols == 1) && (nrows == 1) &&
          (orig_nrows_present) && (orig_ncols_present));
} /* iscompressed() */


VImage decompress(VImage src, VAttrListPosn posn, VLong nrows, VLong ncols, VLong nbands) {
  VImage decompressed=NULL, tmpimg=NULL;
  VLong i,j,k;
  VAttrListPosn startposn=posn;

#ifdef DEBUG
    fprintf(stderr, "%s: decompressing image.\n", __FUNCTION__);
    fprintf(stderr, "%s:nrows:\t%d\n", __FUNCTION__, nrows);
    fprintf(stderr, "%s:ncols:\t%d\n", __FUNCTION__, ncols);
    fprintf(stderr, "%s:nbands:\t%d\n", __FUNCTION__, nbands);
#endif 

  if (nrows==0) {
    if (VAttrFound !=
	VGetAttr(VImageAttrList(src), V_HDR_ORI_NROWS, NULL, VLongRepn, &nrows))
      VError("%s: Couldn't find %s in image", __FUNCTION__, V_HDR_ORI_NROWS);
  }
  if (ncols==0) {
    if (VAttrFound !=
	VGetAttr(VImageAttrList(src), V_HDR_ORI_NCOLS, NULL, VLongRepn, &ncols))
      VError("%s: Couldn't find %s in image", __FUNCTION__, V_HDR_ORI_NCOLS);
  }
  if (nbands==0) {
    /* continue searching attr-list, until uncompressed image is found */
    for(VNextAttr(&posn); VAttrExists(&posn); VNextAttr(&posn)) {
      if (VGetAttrRepn(&posn) != VImageRepn) continue;
      if (!(VGetAttrValue(&posn, NULL, VImageRepn, &tmpimg)))
	VWarning("%s: Couldn't extract image. Skipping", __FUNCTION__);
      if (!tmpimg) continue;
      if (iscompressed(tmpimg)) continue;
      if ((VImageNRows(tmpimg) != nrows) || (VImageNColumns(tmpimg) != ncols)) continue;
      nbands=VImageNBands(tmpimg);
    }
    if (nbands == 0) { /* no value for nbands found, try backwards in list */
      posn=startposn;
      for(VPrevAttr(&posn); VAttrExists(&posn); VPrevAttr(&posn)) {
	if (VGetAttrRepn(&posn) != VImageRepn) continue;
	if (!(VGetAttrValue(&posn, NULL, VImageRepn, &tmpimg)))
	  VWarning("%s: Couldn't extract image. Skipping", __FUNCTION__);
	if (!tmpimg) continue;
	if (iscompressed(tmpimg)) continue;
	if ((VImageNRows(tmpimg) != nrows) || (VImageNColumns(tmpimg) != ncols)) continue;
	nbands=VImageNBands(tmpimg);
      }
    }
  }

#ifdef DEBUG
  fprintf(stderr, "%s:nrows:\t%d\n", __FUNCTION__, nrows);
  fprintf(stderr, "%s:ncols:\t%d\n", __FUNCTION__, ncols);
  fprintf(stderr, "%s:nbands:\t%d\n", __FUNCTION__, nbands);
#endif

  decompressed=VCreateImage(nbands, nrows, ncols, VPixelRepn(src));
  if(decompressed) {
    for(i=0; i < nbands; i++) {
      for(j=0; j < nrows; j++) {
	for(k=0; k < ncols; k++) {
	  VSetPixel(decompressed, i, j, k, VGetPixel(src, 0, 0, 0));
	} /* columns */
      } /* rows */
    } /* bands */
    VCopyImageAttrs(src, decompressed);
  } else {
    VError("%s: Couldn't create decompressed image", __FUNCTION__);
  }

  return decompressed;
} /* decompress() */
