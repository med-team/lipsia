/****************************************************************
 *
 * imagetools.h: useful stuff for vista images
 *
 * MPI of Cognitive Neuroscience, Leipzig
 *
 * Till D�rges, March 2001, <doerges@cns.mpg.de>
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges -- 06/03/2001 -- created (restructured code)
 *
 *  Till D�rges -- 14/03/2001 -- added hack for being not strict
 *    when identifying functional images
 *
 *****************************************************************/

#ifndef IMAGETOOLS_H
#define IMAGETOOLS_H

#include <viaio/VImage.h>


int isanaimg(VImage image);

/* True if functional image is detected.  At MPI of CNS usally
   VShort-voxels.
   In addition the name-attribute has to be <string> <int>-<int>.*/
int isfunctimgstrict(VImage image);

/* Like isfunctimgstrict(), but w/o check of name-attr. */
int isfunctimg(VImage image);

int iszmap(VImage image);

/* Images can be "compressed", if all voxels are of the same
   value. Then the compressed image contains exactly one voxel w/ that
   value. V_HDR_ORI_NROWS and V_HDR_ORI_NCOLS (defined in
   vtoanalyze.h) are set accordingly. Unfortunately the programmer has
   to figure out the original no. of bands himself. (Don't blame me
   for that, though.) */
int iscompressed(VImage image);


/* Tries to decompress src, giving the new image nrow rows etc, as
   well as all the attributes of src. If nrow, ncol, nband are set to
   0, decompress() will try to get reasonable values itself.

   posn is needed to continue searching the attribute list from which
   src was extracted in order to retrieve the correct no. of
   bands. Thus posn usually is the position within the attr-list at
   which src was found.

   You might want to think about replacing src in the attr-list by the
   decompressed image (something like: posn.ptr->value=decompressed;
   VDestroyImage(src);)

   Returns NULL upon error, the allocated image otherwise (caller
   must free).*/
VImage decompress(VImage src, VAttrListPosn posn, VLong nrows, VLong ncols, VLong nbands);

#endif /* IMAGETOOLS_H */
