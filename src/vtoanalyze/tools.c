/****************************************************************
 *
 * tools.c: Defines some useful things
 * 
 * MPI of Cognitive Neuroscience, Leipzig
 *
 * (c) by Till D�rges, Jan. 2001, <doerges@cns.mpg.de>
 * (c) for some functions by R. Nowagk, 1998
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges -- 26/01/2001 -- started
 *  Till D�rges -- 14/02/2001 -- added basename(), dirname()
 *  Till D�rges -- 15/02/2001 -- debugged basename(), dirname()
 *  Till D�rges -- 20/02/2001 -- added replacesuffix()
 *  Till D�rges -- 06/03/2001 -- restructured code
 *
 *****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

#include <viaio/Vlib.h>
#include <viaio/VImage.h>

#include "tools.h"


int debugfprintf(int threshold, int debuglevel, FILE *f, char *fmt, ...) {
  va_list ap;
  int written=0;

  if (debuglevel >= threshold) {
    va_start(ap, fmt);
    written=vfprintf(f, fmt, ap);
    va_end(ap);
  }
  return written;
} /* debugprintf() */


char *basename(char *path, char *dest, char *suffix) {
  char *base, *localpath;
  char *pos1, *pos2;

  localpath=(char*) calloc(strlen(path) + 1, sizeof(char));
  if (!localpath) return NULL;
  strcat(localpath, path);

  base=strrchr(localpath, '/');
  if (base) base++;            /* base was on '/' */
  else      base=localpath;    /* no dir's in path */

  /* check, whether to chop suffix */
  if(suffix && (strlen(base) >= strlen(suffix))) {
    pos1=base+strlen(base)-strlen(suffix);
    pos2=suffix;

    for(; (*pos1 == *pos2) && (pos2 < (suffix+strlen(suffix))); pos1++, pos2++);

    if ((*pos1 == '\0') && (*pos2 == '\0'))
      *(base+strlen(base)-strlen(suffix))='\0';
  }

  if (dest) strcat(dest, base);
  free(localpath); localpath=NULL;
  return dest;
} /* basename() */


char *dirname(char *path, char *dest) {
  char *pos, *localpath;

  localpath=(char*) calloc(strlen(path) + 1, sizeof(char));
  if (!localpath) return NULL;
  strcat(localpath, path);

  pos=strrchr(localpath, '/');
  if (!pos)
    dest[0]='\0';
  else {
    *++pos='\0';
    if (dest)
      strcat(dest, localpath);
  }

  free(localpath); localpath=NULL;
  return dest;
} /* dirname() */


char *replacesuffix(char* oldpath, char **newpath, char *oldsuffix, char *newsuffix) {
  size_t len;

  len=strlen(oldpath) + strlen(newsuffix) + 1;
  if ((*newpath = (char*) calloc(len, sizeof(char)))) {
    if (!dirname(oldpath, *newpath)) return NULL;
    if (!basename(oldpath, *newpath+strlen(*newpath), oldsuffix)) return NULL;
    strcat(*newpath, newsuffix);
  }
  return *newpath;
} /* replacesuffix() */


/* --------------------- Byteswapping Code ----------------------- */

int must_swap_int16(int first) {
  short x = 42;
  unsigned char *buf = (unsigned char *) &x;
  int is_first = (*buf == 42) ? WRITE_HIGH_LAST : WRITE_HIGH_FIRST;
  
  return (first != is_first);
} /* must_swap_int16() */

int must_swap_float(int first) {
  float x = 8.0;
  unsigned char *buf = (unsigned char *) &x;
  int is_first = (*buf == 0x41) ? WRITE_EXP_FIRST : WRITE_EXP_LAST;
  
  return (first != is_first);
} /* must_swap_float() */


void swap_float(float *val) {
  unsigned char *buf;
  unsigned char  tmp;

  buf = (unsigned char *) val;
  tmp = *buf; *buf = *(buf+3); *(buf+3) = tmp;
  tmp = *(buf+1); *(buf+1) = *(buf+2); *(buf+2) = tmp;
} /* swap_float() */


/* misc vista convenience funcs ------------------------------------------ */

VImage read_first_image(char *filename) {
  FILE *f;
  int i, n;
  VAttrList list;
  VImage *src, tmp;

  f = fopen(filename, "rb");
  if (!f)
    VSystemError("cannot open file %s", filename);
  n = VReadImages(f, &list, &src);
  fclose(f);
  if (n == 0)
    VError("%s: cannot read image(s) from file %s", __FUNCTION__, filename);

  /* throw away any unneeded data */
  VDestroyAttrList(list);
  for (i = 1; i < n; i++)
    VDestroyImage(src[i]);
  tmp = src[0];
  free(src);

  return tmp;
} /* read_first_image() */


VAttrList read_gz_list(char *fname) {
  VAttrList list;
  FILE      *f;
  int       is_pipe = 0;
  char      cmd[1024];
  
  if (strlen(fname) > 3 && !strcmp(&fname[strlen(fname) - 3], ".gz")) {
    sprintf(cmd, "gunzip -c %s", fname);
    f = popen(cmd, "r");
    is_pipe = 1;
  }
  else {
    f = fopen(fname, "r");
    is_pipe = 0;
  }
  if (!f)
    VSystemError("cannot open file %s", fname);
  if (    !(list = VReadFile(f, 0))
       || (is_pipe && pclose(f))
       || (!is_pipe && fclose(f))    )
    VSystemError("cannot read file %s", fname);
  
  return list;
} /* read_gz_list() */
