/****************************************************************
 *
 * tools.h: Defines some useful things
 * 
 * MPI of Cognitive Neuroscience, Leipzig
 *
 * (c) by Till D�rges, 2001, <doerges@cns.mpg.de>
 * (c) for some functions by R. Nowagk, 1998
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges -- 26/01/2001 -- created
 *  Till D�rges -- 14/02/2001 -- added basename(), dirname()
 *  Till D�rges -- 20/02/2001 -- added replacesuffix()
 *  Till D�rges -- 06/03/2001 -- restructured code
 *
 *****************************************************************/
#ifndef TOOLS_H
#define TOOLS_H

#include <stdio.h>
#include <viaio/Vlib.h>

typedef int bool;
#define true  1
#define false 0


/* Writes fmt (+args) to f, iff debuglevel >= threshold.
   Returns number of chars written. */
int debugfprintf(int threshold, int debuglevel, FILE *f, char *fmt, ...);


/* Extracts the basename from path, and puts it into dest. dest must
   point to an area w/ enough free memory (at most strlen(path)).

   If suffix != NULL, it's also stripped from the end (if present):

   Returns dest upon success, NULL upon failure. */
char *basename(char *path, char *dest, char *suffix);


/* Extracts the dirname from path, and puts it into dest. dest must
   point to an area w/ enough free memory (at most
   strlen(path)-strlen(base)).

   dirname is left w/ trailing '/' or is the empty string, if there is
   no dir in the path.

   Returns dest upon success, NULL upon failure. */
char *dirname(char *path, char *dest);


/* Allocates newpath (caller must free) and tries to replace oldsuffix
   by newsuffix. If oldsuffix can't be found, newsuffix is simply
   appended.

   Returns newpath on succes and NULL on error. */
char *replacesuffix(char* oldpath, char **newpath, char *oldsuffix, char *newsuffix);



/* --------------------- Byteswapping Code ----------------------- */
#define WRITE_HIGH_FIRST 0
#define WRITE_HIGH_LAST  1
#define WRITE_EXP_FIRST 0
#define WRITE_EXP_LAST  1

/* Whether signed short ints have to be swapped to be in big-endian-order. */
int must_swap_int16(int first);
int must_swap_float(int first);
void swap_float(float *val);


/* ------------------ misc vista conv. funcs -------------------- */
VAttrList read_gz_list(char *fname);
VImage    read_first_image(char *filename);

#endif /* TOOLS_H */
