/****************************************************************
 *
 * vtoanalyze.c: converts Vista to Analyze 7.x (actually SPM)
 * 
 * Copyright (C) Max Planck Institute 
 * for Human Cognitive and Brain Sciences, Leipzig
 *
 * Authors R. Nowagk, T. D�rges, H. Mentzel, K. Mueller, <lipsia@cbs.mpg.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 * Originally by  R. Nowagk,        1998
 * Modified   by  Till D�rges, Jan. 2001, <doerges@cns.mpg.de>
 * Image orientation and functional data: K. Mueller, 2006
 *
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges   -- 11/01/2001 -- started documenting/hacking the code
 *  Till D�rges   -- 16/01/2001 -- added float (zmap) conversion
 *                              -- corrected bug in swap_float()
 *  Till D�rges   -- 17/01/2001 -- splitted write_hdr() in write & convert
 *  Till D�rges   -- 22/01/2001 -- minor cosmetic modifications
 *  Till D�rges   -- 06/03/2001 -- restructured code, introduced debuglevel
 *  Till D�rges   -- 14/03/2001 -- added hack for being not strict
 *                                 when identifying functional images
 *  Till D�rges   -- 26/03/2001 -- simplified write_img(), removed some warnings
 *  Heiko Mentzel -- 07/27/2001 -- std.output, header
 *
 *****************************************************************/

#include <stdio.h>
#include <stdlib.h>
#ifndef __LCLINT__
#include <netinet/in.h>   /* for byte-swapping */
#endif /*__LCLINT__*/
#include <limits.h>

#include <viaio/Vlib.h>
#include <viaio/mu.h>
#include <viaio/file.h>
#include <viaio/option.h>
#include <viaio/VImage.h>


/* Attention! Several versions are floating around. I need the one w/
   "float roi_scale" at byte-offset 72 in image_dimension. */
#include "dbh.h"               /* Analyze header */

#include "tools.h"             /* useful things */
#include "imagetools.h"        /* handling images */
#include "fisort.h"            /* handling functional series */
#include "vtoanalyze.h"        /* defines */


#define PROGNAME         "vtoanalyze"

#define DL_LITTLE       1     /* elementary progress. */
#define DL_MORE         2     /* converted values */
#define DL_EVENMORE     3     /* detailed progress */
#define DL_MOST         4
#define DL_SICK         5

char program_id[] = "vtoanalyze: $Revision: 3268 $ $Date: 2008-04-11 19:40:12 +0200 (Fri, 11 Apr 2008) $ (C) MPI-CNS";

extern char * getLipsiaVersion();

int vrepn2anarepn(VRepnKind vr) {
  int ar=DT_UNKNOWN;

  switch(vr) {
  case VUByteRepn:  ar = DT_UNSIGNED_CHAR; break;
  case VShortRepn:  ar = DT_SIGNED_SHORT;  break;
  case VFloatRepn:  ar = DT_FLOAT; break;
  case VDoubleRepn: ar = DT_DOUBLE; break;
  default:
    VError("%s: Image representation \"%s\" not supported. Sorry",
	   __FUNCTION__, VRepnName(vr));
  }
  return ar;
} /* vrepn2anarepn() */


/* Converts and writes img to f. Forces high-byte first (BIG-ENDIAN).
   dl is the debuglevel.
   Returns boolean indicating success. 
*/
int write_img(FILE *f, int ana_repn, VImage img, int dl, VBoolean flipx, VBoolean flipy, VBoolean flipz) {
  VPointer src=NULL;   /* img-data in machine-dependent format */
  VPointer dest=NULL;  /* packed img-data (actually the same as src) */
  VImage destin=NULL;
  size_t srclen=0;     /* no. of bytes allocated */
  size_t destlen=0;
  size_t nvox=0;       /* no. of voxels in img */
  size_t packedsize=0; /* space needed for packed data in bytes */

  int anabitspervoxel=0, vbitspervoxel;
  VRepnKind vrepn=VPixelRepn(img);
  size_t res;
  VImage swap, wrap;
  int nbands, nrows, ncols;
  int i, j, k , r, c, b, bb, npix;
  FILE *fp = NULL;
  VUByte *srcu_pp=NULL, *destu_pp=NULL;
  VFloat *srcf_pp=NULL, *destf_pp=NULL;
  VShort *srcs_pp=NULL, *dests_pp=NULL;


  /* Create image */
  destin = VCopyImage(img, NULL, VAllBands);
  
  /* Superior to Inferior ??? */
  if (flipz) { 
    VFillImage(destin,VAllBands,0);
    bb = VImageNBands(destin)-1;
    for (b=0; b<VImageNBands(destin); b++) {
      for (r=0; r<VImageNRows(destin); r++) {
	for (c=0; c<VImageNColumns(destin); c++) 
	  VSetPixel(destin, bb, r, c, VGetPixel(img, b, r, c)); 
      }
      bb--;
    }
    VCopyImage(destin, img, VAllBands);
  } 
  
  /* Anterior to Posterior ??? */
  if (flipy) {
    VFillImage(destin,VAllBands,0);   
    VFlipImage(img, destin,VAllBands,TRUE);
    VCopyImage(destin, img, VAllBands);
  }
  
  /* Right to Left ??? */
  if (flipx) {
    VFillImage(destin,VAllBands,0);   
    VFlipImage(img,destin,VAllBands,FALSE); 
    VCopyImage(destin, img, VAllBands);
  } 
  
  /* Free src Image */
  VDestroyImage(destin);

  /* set some sizes/lengths */
  switch (ana_repn) {
  case DT_UNSIGNED_CHAR: anabitspervoxel=8;  break;
  case DT_SIGNED_SHORT:  anabitspervoxel=16; break;
  case DT_FLOAT:         anabitspervoxel=32; break;
  case DT_DOUBLE:        anabitspervoxel=64; break;
  default:
    VError("%s: Image representation %d (Analyze) not supported. Sorry",
	   __FUNCTION__, ana_repn);
    break;
  }
  vbitspervoxel=VRepnPrecision(vrepn);  /* machine INdependent */
  nvox=VImageNPixels(img);
  packedsize=nvox*vbitspervoxel/CHAR_BIT;
  srclen=nvox*VPixelSize(img);          /* machine dependent */

  /* sanity checks */
  if (anabitspervoxel !=  vbitspervoxel) {
    VError("%s: Analyze requires %d bits per voxel, vista has %d",
	   __FUNCTION__, anabitspervoxel, vbitspervoxel);
  }

  /* get raw data in machine dependent form (must contain srclen bytes) */
  if(!(src=VImageData(img))) VError("%s: Couldn't retrieve image-data", __FUNCTION__);





  /* force to big-endian */
  destlen=srclen; dest=src;
  VPackData(vrepn, nvox, src, VMsbFirst, &destlen, &dest, NULL);

  if(dest != src)
    VWarning("%s: Packing should have been done in the same place", __FUNCTION__);
  if(destlen != packedsize)
    VWarning("%s: Packed data takes %d bytes instead of %d", __FUNCTION__, destlen, packedsize);


  res=fwrite(dest, 1, destlen, f);

  return (res == destlen);
} /* write_img() */



/* Forces Analyze-hdr hdr to big-endian. (Network-representation in
   TCP is big-endian.)

   dl is a debuglevel. */
void big_endian_hdr(struct dsr *hdr, int dl) {
  int swapint   = must_swap_int16(WRITE_HIGH_FIRST);
  int swapfloat = must_swap_float(WRITE_EXP_FIRST);

  int16_t ca[3];

  if (swapint) {
    debugfprintf(DL_EVENMORE, dl, stdout,
		 "%s: Swapping ints in hdr.\n", __FUNCTION__);
    hdr->hk.sizeof_hdr=htonl(hdr->hk.sizeof_hdr);
    hdr->hk.extents=htonl(hdr->hk.extents);

    hdr->dime.dim[0]=htons(hdr->dime.dim[0]);
    hdr->dime.dim[1]=htons(hdr->dime.dim[1]);
    hdr->dime.dim[2]=htons(hdr->dime.dim[2]);
    hdr->dime.dim[3]=htons(hdr->dime.dim[3]);
    hdr->dime.dim[4]=htons(hdr->dime.dim[4]);

    hdr->dime.datatype=htons(hdr->dime.datatype);
    hdr->dime.bitpix=htons(hdr->dime.bitpix);

    /* swap ant. commissure */
    memcpy(ca, hdr->hist.originator, 3*sizeof(ca[0]));
    ca[0]=htons(ca[0]);
    ca[1]=htons(ca[1]);
    ca[2]=htons(ca[2]);
    memcpy(hdr->hist.originator, ca, 3*sizeof(ca[0]));
  }

  if (swapfloat) {
    debugfprintf(DL_EVENMORE, dl, stdout,
		 "%s: Swapping floats in hdr.\n", __FUNCTION__);
    swap_float(&hdr->dime.pixdim[0]);
    swap_float(&hdr->dime.pixdim[1]);
    swap_float(&hdr->dime.pixdim[2]);
    swap_float(&hdr->dime.pixdim[3]);
    swap_float(&hdr->dime.pixdim[4]);

    swap_float(&hdr->dime.vox_offset);
    swap_float(&hdr->dime.roi_scale);
  }
} /* big_endian_hdr() */


/* Writes Analyze-hdr hdr to f.
   dl is a debuglevel. */
int write_hdr(FILE *f, struct dsr *hdr, int dl) {
  size_t res;

  res=fwrite(hdr, sizeof(char), sizeof(*hdr), f);
  return ( res == sizeof(*hdr) );
} /* write_hdr() */



/* Takes information from attr-list of img into hdr. Since not all
   info can be placed there, the remainder goes into f (already open).
   dl is a debuglevel.

   Only uses entries used by SPM!

   ana_repn:  see dbh.h
   scale:     roi_scale (coefficient for each voxel)
              (unused at the moment)

   Returns boolean indicating success. 
*/
int convert_headers(FILE *f, struct dsr *hdr, VImage img,
		    int ana_repn, float scale, int dl, int no_volumes, VBoolean isSPM2, VBoolean flipx, VBoolean flipy, VBoolean flipz) {
  VAttrList     attrlist;
  VAttrListPosn posn;
  VStringConst  attrname, attrvalue;

  VString       castr, cpstr, namestr, pixdimstr, buf, str;
  VRepnKind     vrepn;

  float cafloat[3];              /* for reading from vista   */
  int16_t caint[3];              /* for writing into analyze */
  float cpfloat[3];
  int16_t cpint[3];
  double tr=0;

  bzero(hdr, sizeof(struct dsr));
  attrlist=VImageAttrList(img);

  /* Write header attributes known to SPM. */

  hdr->hk.sizeof_hdr=(int) sizeof(*hdr);    /* screwed, if int isn't 4 bytes... */
  hdr->hk.extents=16384;
  hdr->hk.regular='r';
  hdr->hk.hkey_un0 = '0';

  hdr->dime.dim[0] = 4;
  hdr->dime.dim[1] = (short int) VImageNColumns(img); /* x */
  hdr->dime.dim[2] = (short int) VImageNRows(img);    /* y */
  hdr->dime.dim[3] = (short int) VImageNBands(img);   /* z */
  if (isSPM2)
    hdr->dime.dim[4] = (short int) no_volumes;  
  else
    hdr->dime.dim[4] = (short int) 1;  

  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Set dimensions to x%d, y%d, z%d (voxels).\n",
	       __FUNCTION__, hdr->dime.dim[1], hdr->dime.dim[2], hdr->dime.dim[3]);

  hdr->dime.datatype=ana_repn;

  snprintf(hdr->dime.vox_units, 3, "%s", "mm");

  switch (ana_repn) {
  case DT_UNSIGNED_CHAR: hdr->dime.bitpix = 8;  break;
  case DT_SIGNED_SHORT:  hdr->dime.bitpix = 16; break;
  case DT_FLOAT:         hdr->dime.bitpix = 32; break;
  case DT_DOUBLE:        hdr->dime.bitpix = 64; break;
  default:  VError("%s: Image representation %d not supported by Analyze. Sorry", __FUNCTION__, ana_repn);
  }

  snprintf(hdr->hist.aux_file, 5, "%s", "none");

  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Set Analyze-datatype to %d.\n",
	       __FUNCTION__, hdr->dime.datatype);


  hdr->dime.pixdim[0]=(float) 3;      /* Voxel dims. in mm. */
  if (   (VGetAttr(attrlist, V_HDR_VOXEL , NULL, VStringRepn, &pixdimstr)
	  != VAttrFound) 
      || (sscanf(pixdimstr, "%f%f%f",
		 &hdr->dime.pixdim[2], &hdr->dime.pixdim[1], &hdr->dime.pixdim[3])
	  != 3)  ) {
    VWarning("\n\tHeader \"%s\" missing? Can't read voxel dimensions!\n\tForcing to x1.0, y1.0, z1.0 (mm)", V_HDR_VOXEL);

    hdr->dime.pixdim[1]=(float) 1.0;
    hdr->dime.pixdim[2]=(float) 1.0;
    hdr->dime.pixdim[3]=(float) 1.0;
  }

  /* Sauerei!!!!!!!!! */
  /* hdr->dime.pixdim[1] *= -1; soll Fehler in SPM2 ausgleichen */
  /* Sauerei!!!!!!!!! */

  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Set voxel-dimensions to x%f, y%f, z%f (mm).\n", __FUNCTION__,
	       hdr->dime.pixdim[1], hdr->dime.pixdim[2], hdr->dime.pixdim[3]);

  /*
  ** read repetition time
  */
  tr = 0;
  if (VGetAttr (attrlist, "repetition_time", NULL,
		VDoubleRepn, (VPointer) & tr) != VAttrFound) {
    tr = 0;
    buf = VMalloc(256);
    if (VGetAttr (attrlist, "MPIL_vista_0", NULL,
		  VStringRepn, (VPointer) & str) == VAttrFound) {
      sscanf(str," repetition_time=%lf %s",&tr,buf);
    }
  }
  if (tr > 1) 
    hdr->dime.pixdim[4]=(float) tr/1000.0;


  hdr->dime.vox_offset=(float) 0;  /* byte-offset of image data in *.img */

  hdr->dime.roi_scale=scale;
  debugfprintf(DL_MORE, dl, stdout, "%s: Set scale to %f.\n", __FUNCTION__, hdr->dime.roi_scale);


  if (   (VGetAttr(attrlist, V_HDR_CA, NULL, VStringRepn, &castr)
          != VAttrFound)
      || (sscanf(castr, "%f %f %f", &cafloat[1], &cafloat[0], &cafloat[2])) != 3) {
    caint[0] = 0; 
    caint[1] = 0; 
    caint[2] = 0;
  } else {
    /* ist das so richtig ???? */
    caint[0]=VImageNColumns(img)-(int)(cafloat[1]+1);
    caint[1]=VImageNRows(img)-(int)(cafloat[0]+1);
    if (flipz==TRUE)
      caint[2]=VImageNBands(img)-(int)(cafloat[2]+1); 
    else
      caint[2]=cafloat[2]+1;



    /* von Till Doerges --- falsch?
    caint[0]=cafloat[0]+1;
    caint[1]=cafloat[1]+1;
    caint[2]=cafloat[2]+1; */
  }

  

  memcpy(hdr->hist.originator, caint, 3*sizeof(caint[0]));
  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Set ac to x%d, y%d, z%d (voxels).\n", __FUNCTION__,
	       caint[0], caint[1], caint[2]);

  if (VGetAttr(attrlist, V_HDR_NAME, NULL, VStringRepn, &namestr) != VAttrFound) 
    strncat(hdr->hist.descrip, "lipsia", ANA_HDR_DESC_LEN-sizeof(hdr->hist.descrip)-1);
  else
    strncat(hdr->hist.descrip, namestr, ANA_HDR_DESC_LEN-sizeof(hdr->hist.descrip)-1);

  debugfprintf(DL_MORE, dl, stdout,
	       "%s: Set description to \"%s\".\n", __FUNCTION__, hdr->hist.descrip);


  debugfprintf(DL_EVENMORE, dl, stdout,
	       "%s: Writing attributelist to file.\n", __FUNCTION__);

  fprintf(f, "# Not all image attributes could be saved in the Analyze file!\n");
  /* Loop through attributes of image and write them to f */
  for(VFirstAttr (attrlist, &posn); VAttrExists(&posn); VNextAttr (&posn)) {
    attrname = VGetAttrName(&posn);

    switch(VGetAttrRepn(&posn)) {
    case VStringRepn:
      if (! VGetAttrValue(&posn, NULL, VStringRepn, (VPointer) &attrvalue)) {
	VWarning("Couldn't get value for attribute \"%s\". Skipping", attrname);
      } else {

	
	/* Special treatment of some headers! */

	fprintf(f, "%s: %s\n", attrname, attrvalue);
	debugfprintf(DL_MOST, dl, stdout,
		     "%s: Wrote: \"%s: %s\"\n", __FUNCTION__, attrname, attrvalue);
      }
      break;

    default:
      VWarning("Don't know how to handle attribute \"%s\". Skipping", attrname);
    } /* switch */

  } /* for */
  
  return 1;  /* end w/ success */
} /* convert_headers() */



/*
   Processes all (!) series of (functional) images.

   list has to be initialized (i.e. by VReadFile()) and is searched for
   multiple series of scans.
   out_stub is used to generate file-names for the newly written
   images.
   imagetype is IMAGE_FUNCTIONAL only for the moment.
   strict denotes, whether to be picky when identifying objects
    (yes/true is recommended).
   dl is a debuglevel.

   A functional image is spread over several objects. Each
   object/attribute contains dimensions (x,y,t). The objects itself
   represents (z).

   x = ncols
   y = nrows
   t = nbands (increasing)
   z is decreasing (first object: highest z-value) 
*/
VBoolean
process_serie(VAttrList list, VStringConst out_stub,
		   int imagetype, VBoolean strict, int dl, VBoolean isSPM2, VBoolean flipx, VBoolean flipy, VBoolean flipz) {
  VImage     src;
  VRepnKind  repn;
  VString    str;
  int        nimages = 0;
  int        nvolumes = 0;

  VBoolean wroteHEADER = FALSE;
  VBoolean orientationproblem = FALSE;

  float scale=1;             /* scale for analyze, currently not supported */
  struct dsr hdr;            /* Analyze Header */
  int ana_repn=DT_UNKNOWN;

  FILE *imgout=NULL, *hdrout=NULL, *txtout=NULL;
  FILE *out=NULL;

  int outsize   = strlen(out_stub) + MAX_STUB_END + 1;
  char *outname = (char *) calloc(outsize, sizeof(char));


  scanlist_t *scanv=NULL, *s=NULL;
  int scanc = 0, scan = 0, frame = 0;


  switch(imagetype) {
  case IMAGE_FUNCTIONAL:
    debugfprintf(DL_EVENMORE, dl, stdout,
	       "%s: Assembling functional series...\n", __FUNCTION__);
    scanv = scanlist_init(list, &scanc, strict);
    break;
  default:
    VError("%s: Internal program error (wrong image-type)", __FUNCTION__);
    break;
  }

  if (isSPM2 == TRUE) {

    /* open img file for continuous writing: */
    sprintf(outname, "%s%s", out_stub, ANA_IMG_SUFFIX);
    imgout = fopen(outname, "wb");
    if (! imgout) {
      VSystemError("%s: Cannot write output file \"%s\"",
		   __FUNCTION__, outname);
    }
    debugfprintf(DL_MOST, dl, stdout,
		 "%s: Writing file %s.\n", __FUNCTION__, outname);
    
    /* iterate through all time-steps (frames/volumes) of all scan-series */
    for (scan = 0; scan < scanc; scan++) {

      s = &scanv[scan];
      debugfprintf(DL_LITTLE, dl, stdout, "%s: Writing %d timesteps for scan %d.\n", 
		   __FUNCTION__, s->nframes, scan);
      if (nvolumes == 0) 
	nvolumes = s->nframes;

      for (frame = 0; frame < s->nframes; frame++) {
	src = scanlist_get_volume(scanv, scan, frame);

      	if (!isfunctimg(src)) continue;
	
	/* check talairach */
	if (VGetAttr(VImageAttrList(src),"talairach",NULL,VStringRepn,(VPointer)&str)==VAttrFound && !flipz) 
	  orientationproblem = TRUE;
	if (VGetAttr(VImageAttrList(src),"talairach",NULL,VStringRepn,(VPointer)&str)!=VAttrFound && flipz) 
	  orientationproblem = TRUE;

	/* do it only once: */
	if (wroteHEADER == FALSE) {
	  
	  /* find translation */
	  ana_repn = vrepn2anarepn(repn=VPixelRepn(src));
	  
	  /* convert headers, dump additonal infos into *.txt */
	  sprintf(outname, "%s%s", out_stub, ANA_TXT_SUFFIX);
	  debugfprintf(DL_LITTLE, dl, stdout,
		       "%s: Converting and writing headers to file %s.\n"
		       , __FUNCTION__, outname);
	  
	  txtout = fopen(outname, "wb");
	  if ( !txtout
	       || !convert_headers(txtout, &hdr, src, ana_repn, scale, dl, nvolumes, isSPM2, flipx, flipy, flipz)
	       || fclose(txtout)) {
	    VSystemError("%s: Cannot write output file \"%s\"",
			 __FUNCTION__,  outname);
	  }
	  
	  /* force hdr to big-endian */
	  debugfprintf(DL_MOST, dl, stdout,
		       "%s: Forcing Analyze-header to big-endian.\n", __FUNCTION__);
	  big_endian_hdr(&hdr, dl);


	  /* dump *.hdr file */
	  sprintf(outname, "%s%s", out_stub, ANA_HDR_SUFFIX);
	  hdrout = fopen(outname, "wb");
	  if (! hdrout
	      || !write_hdr(hdrout, &hdr, dl)
	      || fclose(hdrout)  ) {
	    VSystemError("%s: Cannot write output file \"%s\"", __FUNCTION__, outname);
	  }

	  wroteHEADER = TRUE;
	} /* do it only once */

	
	
	/* force img to big-endian and dump *.img file */
	if (!write_img(imgout, ana_repn, src, dl, flipx, flipy, flipz)) {
	  VSystemError("%s: Cannot write output file \"%s\"",
		       __FUNCTION__, outname);
	}
	    
	VDestroyImage(src);

      } /* for frame */
    }  /* for scan */
  
    fclose(imgout);
  }
  else {
  /* iterate through all time-steps (frames) of all scan-series */
  for (scan = 0; scan < scanc; scan++) {
    nimages = 0;
    s = &scanv[scan];
      if (nvolumes == 0) 
	nvolumes = s->nframes;
      
    debugfprintf(DL_LITTLE, dl, stdout, "%s: Writing %d timesteps for scan %d.\n"
		 , __FUNCTION__, s->nframes, scan);
    for (frame = 0; frame < s->nframes; frame++) {
      src = scanlist_get_volume(scanv, scan, frame);

      /* check talairach */
      if (VGetAttr(VImageAttrList(src),"talairach",NULL,VStringRepn,(VPointer)&str)==VAttrFound && !flipz) 
	orientationproblem = TRUE;
      if (VGetAttr(VImageAttrList(src),"talairach",NULL,VStringRepn,(VPointer)&str)!=VAttrFound && flipz) 
	orientationproblem = TRUE;

      /* only for first image/frame of a serie/scan */
      if (nimages == 0) {

	/* find translation */
        ana_repn=vrepn2anarepn(repn=VPixelRepn(src));

	/* convert headers, dump additonal infos into *.txt */
	sprintf(outname, "%s%0*d%s", out_stub, WIDTH, nimages, ANA_TXT_SUFFIX);
	debugfprintf(DL_LITTLE, dl, stdout,
		     "%s: Converting and writing headers to file %s.\n"
		     , __FUNCTION__, outname);

	out = fopen(outname, "wb");
	if (!out
	    || !convert_headers(out, &hdr, src, ana_repn, scale, dl, nvolumes, isSPM2, flipx, flipy, flipz)
	    || fclose(out)) 
	  VSystemError("%s: Cannot write output file \"%s\"",
	    __FUNCTION__,  outname);

	/* force hdr to big-endian */
	debugfprintf(DL_MOST, dl, stdout,
		     "%s: Forcing Analyze-header to big-endian.\n", __FUNCTION__);
	big_endian_hdr(&hdr, dl);
      } /* end of first image of serie */


      /* force img to big-endian and dump *.img file */
      sprintf(outname, "%s%0*d%s", out_stub, WIDTH, nimages, ANA_IMG_SUFFIX);
      debugfprintf(DL_MOST, dl, stdout,
		   "%s: Writing file %s.\n", __FUNCTION__, outname);
      out = fopen(outname, "wb");
      if (    !out
	      || !write_img(out, ana_repn, src, dl, flipx, flipy, flipz)
           || fclose(out)  )
        VSystemError("%s: Cannot write output file \"%s\"",
	  __FUNCTION__, outname);


      /* dump *.hdr file */
      sprintf(outname, "%s%0*d%s", out_stub, WIDTH, nimages, ANA_HDR_SUFFIX);
      debugfprintf(DL_MOST, dl, stdout,
		   "%s: Writing file %s.\n", __FUNCTION__, outname);
      out = fopen(outname, "wb");
      if (    !out
           || !write_hdr(out, &hdr, dl)
           || fclose(out)  )
        VSystemError("%s: Cannot write output file \"%s\"", __FUNCTION__, outname);

      VDestroyImage(src);
      nimages++;

    } /* for frame */
  }  /* for scan */

  } /* else (isSPM2 == TRUE) */

  free(outname);

  return orientationproblem;
} /* process_serie() */


/* Process all (!) single images in list.
   out_stub is used to generate file-names for the newly written
   images.
   imagetype is one of IMAGE_ANATOMIC, or IMAGE_ZMAP.
   dl is a debuglevel.

   An anatomical image or a zmap consist of one object/attribute
   containing (x,y,z).

   x = ncols
   y = nrows
   z = nbands (bottom usally is first)
*/
VBoolean
process_single(VAttrList list, VStringConst out_stub, int imagetype, int dl, VBoolean isSPM2, VBoolean flipx, VBoolean flipy, VBoolean flipz) {
  VImage        src=NULL, compressedimage=NULL;
  VAttrListPosn posn;
  VString       str;
  VRepnKind     repn;
  VBoolean      orientationproblem = FALSE;

  int i = 0;

  int ana_repn = 0;
  float scale=1;             /* scale for analyze, currently not supported */
  struct dsr hdr;            /* analyze header */

  FILE    *out;
  char    *outname = (char *) calloc(strlen(out_stub) + MAX_STUB_END + 1, sizeof(char));


  /* loop through all anatomic images */
  for (i=0, VFirstAttr(list, &posn); VAttrExists(&posn); VNextAttr(&posn)) {

    /* skip non-images */
    if (VGetAttrRepn(&posn) != VImageRepn) continue;
    VGetAttrValue(&posn, NULL, VImageRepn, &src);

    /* skip images that are neiter ana. nor zmap */
    switch(imagetype) {
    case IMAGE_ANATOMIC: if (!isanaimg(src)) continue; break;
    case IMAGE_ZMAP:     if (!iszmap(src))   continue; break;
    default:
      VError("%s: Internal program error (wrong image-type)", __FUNCTION__);
      break;
    }

    /* find translation */
    ana_repn=vrepn2anarepn(repn=VPixelRepn(src));

    /* check, whether image is compressed */
    if (iscompressed(src)) {
      debugfprintf(DL_MOST, dl, stdout,
		   "%s: Decompressing image %d.\n", __FUNCTION__, i);
      compressedimage=src;
      src=decompress(compressedimage, posn, 0,0,0);
      if(!src)
	VError("%s: Couldn't decompress image", __FUNCTION__);
      posn.ptr->value=src; /* hook decompressed image into attr-list */
      VDestroyImage(compressedimage); compressedimage=NULL;
    }
    else {
      debugfprintf(DL_MOST, dl, stdout,
		   "%s: Image is not compressed.\n", __FUNCTION__);
    }

    /* check talairach */
    if (VGetAttr(VImageAttrList(src),"talairach",NULL,VStringRepn,(VPointer)&str)==VAttrFound && !flipz) 
      orientationproblem = TRUE;
    if (VImageNBands(src)<128 && VGetAttr(VImageAttrList(src),"talairach",NULL,VStringRepn,(VPointer)&str)!=VAttrFound && flipz) 
      orientationproblem = TRUE;

    /* convert headers, dump additonal infos into *.txt */
    sprintf(outname, "%s%0*d%s", out_stub, WIDTH, i, ANA_TXT_SUFFIX);
    debugfprintf(DL_LITTLE, dl, stdout,
		 "%s: Converting and writing headers to file %s.\n", __FUNCTION__, outname);

    out = fopen(outname, "wb");
    if (   !out
	   || !convert_headers(out, &hdr, src, ana_repn, scale, dl, 1, isSPM2, flipx, flipy, flipz)
	|| fclose(out))
     VSystemError("%s: Cannot write output file \"%s\"",
       __FUNCTION__, outname);

    /* force hdr to big-endian */
    debugfprintf(DL_EVENMORE, dl, stdout,
		 "%s: Forcing Analyze-header to big-endian.\n", __FUNCTION__);
    big_endian_hdr(&hdr, dl);

    /* force img to big-endian and dump *.img file */
    sprintf(outname, "%s%0*d%s", out_stub, WIDTH, i, ANA_IMG_SUFFIX);
    debugfprintf(DL_LITTLE, dl, stdout,
		 "%s: Writing file %s.\n", __FUNCTION__, outname);
    out = fopen(outname, "wb");
    if (    !out
	    || !write_img(out, ana_repn, src, dl, flipx, flipy, flipz)
         || fclose(out)  )
      VSystemError("%s: Cannot write output file \"%s\"",
	__FUNCTION__, outname);
    
    
    /* dump *.hdr file */
    sprintf(outname, "%s%0*d%s", out_stub, WIDTH, i, ANA_HDR_SUFFIX);
    debugfprintf(DL_LITTLE, dl, stdout,
		 "%s: Writing file %s.\n", __FUNCTION__, outname);
    out = fopen(outname, "wb");
    if (    !out
         || !write_hdr(out, &hdr, dl)
         || fclose(out)  )
      VSystemError("%s: Cannot write output file \"%s\"",
	__FUNCTION__, outname);

    i++;
  } /* for attr */

  free(outname);

  return orientationproblem;
} /* process_single() */ 



int main (int argc, char *argv[]) {

  static VBoolean FOUNDanatomic, FOUNDfunctional, FOUNDzmap;
  static VArgVector avanatomic, avfunctional, avzmap;
  static VBoolean strictfunc=FALSE;
  static VBoolean isSPM2 = TRUE;
  static VBoolean flipx=FALSE;
  static VBoolean flipy=FALSE;	
  static VBoolean flipz=FALSE;
  static VStringConst out_stub;
  static VShort debuglevel=0; //DL_LITTLE;


  static VOptionDescRec options[] = {
    { "out", VStringRepn, 1, &out_stub, VRequiredOpt, NULL,
        "output filename stub" },
    { "anatomic", VBooleanRepn, 0, &avanatomic, &FOUNDanatomic, NULL,
        "convert anatomic image(s)" },
    { "functional", VBooleanRepn, 0, &avfunctional, &FOUNDfunctional, NULL,
        "convert functional image(s)" },
    /* { "strictfunc", VBooleanRepn, 1, &strictfunc, VOptionalOpt, NULL,
        "strict way of identifying functional image(s)" }, */
    { "zmap", VBooleanRepn, 0, &avzmap, &FOUNDzmap, NULL,
        "convert zmap(s)" },
    { "4D", VBooleanRepn, 1, &isSPM2, VOptionalOpt, NULL,
        "4D Analyze format" },
    { "xflip", VBooleanRepn, 1, &flipx, VOptionalOpt, NULL,
      "flip x axis" },    
    { "yflip", VBooleanRepn, 1, &flipy, VOptionalOpt, NULL,
      "flip y axis" },    
    { "zflip", VBooleanRepn, 1, &flipz, VOptionalOpt, NULL,
      "flip z axis" },
    /* { "verbose", VShortRepn, 1, &debuglevel, VOptionalOpt, NULL,
       "a debuglevel"} */
  };

  VAttrList list;
  char *outname;
  FILE *test, *inf;
  VBoolean anatomic, functional, zmap, verbose;
  VBoolean orientationproblem=FALSE;

  char prg_name[50];	
  sprintf(prg_name,"vtoanalyze V%s", getLipsiaVersion());
  
  fprintf (stderr, "%s\n", prg_name);

  /* Parse command line arguments and identify files: */
  VParseFilterCmd(VNumber(options), options, argc, argv, &inf, NULL);
  VSetProgramName(PROGNAME);

  anatomic=functional=zmap=verbose=FALSE;
  flipx = !flipx; flipy = !flipy; flipz = !flipz;

  /* check whether switches had attribute */
  /* Standalone switch = true, switch not set = false */
  if (FOUNDanatomic && (avanatomic.number == 0)) {
    anatomic=TRUE;
  } else if (FOUNDanatomic && (avanatomic.number > 0)) {
    anatomic=((VBoolean *) avanatomic.vector)[0];
  }
  if (FOUNDfunctional && (avfunctional.number == 0)) {
    functional=TRUE;
  } else if (FOUNDfunctional && (avfunctional.number > 0)) {
    functional=((VBoolean*) avfunctional.vector)[0];
  }
  if (FOUNDzmap && (avzmap.number == 0)) {
    zmap=TRUE;
  } else if (FOUNDzmap && (avzmap.number > 0)) {
    zmap=((VBoolean*) avzmap.vector)[0];
  }


  /* test output filename */
  outname = (char *) malloc(strlen(out_stub) + MAX_STUB_END + 1);
  strcpy(outname, out_stub);
  strcat(outname, "0000ANA_IMG_SUFFIX");
  test = fopen(outname, "wb");
  if (!test)
    VSystemError("Cannot create output file \"%s\"!", outname);

  fclose(test);
  remove(outname);
  free(outname);
 
  /* read source image */
  if (!(list = VReadFile(inf, NULL))) VError("Error reading input image");
  fclose(inf);

  if (functional) {
    debugfprintf(DL_LITTLE, debuglevel, stdout,
		 "%s: Processing functional image(s).\n", PROGNAME);
    orientationproblem = process_serie(list, out_stub, IMAGE_FUNCTIONAL, strictfunc, debuglevel, isSPM2, flipx, flipy, flipz);
  }
  if (anatomic) {
    debugfprintf(DL_LITTLE, debuglevel, stdout,
		 "%s: Processing anatomic image(s).\n", PROGNAME);
    orientationproblem = process_single(list, out_stub, IMAGE_ANATOMIC, debuglevel, isSPM2, flipx, flipy, flipz);
  }
  if (zmap) {
    debugfprintf(DL_LITTLE, debuglevel, stdout,
		 "%s: Processing zmap(s).\n", PROGNAME);
    orientationproblem = process_single(list, out_stub, IMAGE_ZMAP, debuglevel, isSPM2, flipx, flipy, flipz);
  }

  /* Cleaning up */
  VDestroyAttrList(list);

  if (orientationproblem)
    VWarning("There might be a problem with the image orientation in the z axis. In Lipsia, 2D slices are ordered upside down, but not in 3D images. Please check the image orientation of all output images. Be sure to use the option '-zflip' in a correct way");
  
  /*debugfprintf(DL_LITTLE, debuglevel, stdout, "%s: Done.\n", PROGNAME); */
  fprintf (stderr, "%s: done.\n",argv[0]);
  return 0;
} /* main */
