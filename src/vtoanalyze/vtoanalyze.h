/****************************************************************
 *
 * vtoanalyze.h: converts Vista to Analyze 7.x (actually SPM)
 * 
 * MPI of Cognitive Neuroscience, Leipzig
 *
 * Till D�rges, Mar. 2001, <doerges@cns.mpg.de>
 *
 ****************************************************************
 *
 * History:
 * ======== 
 *  Till D�rges -- 06/03/2001 -- created (restructured code)
 *
 *****************************************************************/

#ifndef VTOANALYZE_H
#define VTOANALYZE_H


/* stuff for analyze */
#define ANA_HDR_SUFFIX   ".hdr"
#define ANA_IMG_SUFFIX   ".img"
#define ANA_TXT_SUFFIX   ".txt"

#define ANA_HDR_DESC_LEN 80           /* length of this header */

/* stuff for vista */
#define V_HDR_VOXEL      "voxel"      /* keywords to search attr-list */
#define V_HDR_CA         "ca"
#define V_HDR_CP         "cp"
#define V_HDR_NAME       "name"
#define V_HDR_CONVENTION "convention"
#define V_HDR_ORIGIN     "origin"

#define V_HDR_ORI_NROWS  "ori_nrows"  /* for compressed images */
#define V_HDR_ORI_NCOLS  "ori_ncolumns"

#define V_NEUROLOGICAL_CONVENTION  "natural"  /* Key-word in vista-files */
/* Neurological Convention (R is R) */
/* Radiological Convention (L is R) used by SPM */


/* stuff for vtoanalyze itself */
#define IMAGE_ANATOMIC   0     /* zmap & anatomic are of the same structure */
#define IMAGE_FUNCTIONAL 1     /* but w/ different voxel-repn. */
#define IMAGE_ZMAP       2

#define MAX_STUB_END     20    /* max len for end (0000.img etc. is appended to stub) */
#define WIDTH            5     /* No. of digits appended to stub */


#endif /* VTOANALYZE_H */
